package com.momsitter.demo.resource;

import com.momsitter.demo.bo.TimerInfo;
import com.momsitter.models.dao.TimerRuleDAO;
import com.momsitter.models.dao.TimerTaskDAO;
import com.momsitter.models.dao.TimerTaskTargetDAO;
import com.momsitter.models.vo.TimerRuleVO;
import com.momsitter.models.vo.TimerTaskVO;
import com.momsitter.nettyrest.response.Result;
import com.momsitter.nettyrest.StatusCode;
import com.momsitter.nettyrest.ApiProtocol;
import com.momsitter.nettyrest.BaseResource;
import com.momsitter.nettyrest.utils.TimerUtil;
import com.momsitter.sentry.SentryLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.toIntExact;

/**
 * Created by seungjaeyuk on 2017. 8. 27..
 */
public class TimerResource extends BaseResource {
    private static final Logger logger = LoggerFactory.getLogger(TimerResource.class);
    private SentryLogger sentry = null;
    public TimerResource(ApiProtocol apiProtocol) {
    super(apiProtocol);
}

    public Result post() {
        sentry = new SentryLogger();
        sentry.addBreadCrumb("[TimerResource] entry");
        // TODO: 위의 파라메터들은 사용자 한명에 대한 것인데, 여러명을 동시에 등록해야 하는 경우에는 JSON으로 받는 방식으로 구현 필요
        TimerInfo timerInfo = new TimerInfo();
        timerInfo.setTimerType("Test");
        try {
            sentry.addBreadCrumb("[TimerResource] setting parameters");
            String timerRuleId      = getStringParam(apiProtocol, "timerRuleId");
            String userId           = getStringParam(apiProtocol, "userId");
            String reservedDate     = getStringParam(apiProtocol, "reservedDate");
            String reservedMessage  = getStringParam(apiProtocol, "reservedMessage");
            String buttonName       = getStringParam(apiProtocol, "buttonName");
            String buttonLink       = getStringParam(apiProtocol, "buttonLink");
            String relatedUserId    = getStringParam(apiProtocol, "relatedUserId");
            String applyId          = getStringParam(apiProtocol, "applyId");

            if (relatedUserId.equals("")) {
                relatedUserId = "0";
            }
            sentry.setUserId(userId);
            sentry.addExtra("timerRuleId", timerRuleId);
            sentry.addExtra("reservedDate", reservedDate);
            sentry.addExtra("reservedMessage", reservedMessage);
            sentry.addExtra("buttonName", buttonName);
            sentry.addExtra("buttonLink", buttonLink);
            sentry.addExtra("relatedUserId", relatedUserId);
            sentry.addExtra("applyId", applyId);

            sentry.addBreadCrumb("[TimerResource] selectTimerRuleById");
            TimerRuleVO timerRuleVO = new TimerRuleDAO().selectTimerRuleById(Integer.parseInt(timerRuleId));

            if (timerRuleVO == null) {
                throw new Exception("TimerRule is not valid");
            }

            sentry.addBreadCrumb("[TimerResource] insertTimerTask");
            TimerTaskVO timerTaskVO = new TimerTaskDAO().insertTimerTask(Integer.parseInt(timerRuleId), reservedDate);

            if (timerTaskVO == null) {
                throw new Exception("TimerTask is null");
            }

            if (reservedMessage == null) {
                throw new Exception("reservedMessage is null");
            }

            sentry.addBreadCrumb("[TimerResource] insertTimerTaskTarget");
            new TimerTaskTargetDAO().insertTimerTaskTarget(
                    timerTaskVO.getTimerTaskId(),
                    Integer.parseInt(userId),
                    reservedMessage,
                    buttonName,
                    buttonLink,
                    Integer.parseInt(relatedUserId)
            );

            sentry.addBreadCrumb("[TimerResource] calculateReserveTime");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            String reservedDateStr = timerTaskVO.getReservedDate();
            LocalDateTime parsedReservedDate = LocalDateTime.parse(reservedDateStr, formatter);
            long durationMin = Duration.between(now, parsedReservedDate).toMinutes();

            sentry.addExtra("now", now.toString());
            sentry.addExtra("reservedDateStr", reservedDateStr);
            sentry.addExtra("durationMin", durationMin);
            sentry.addExtra("messageType", timerRuleVO.getMessageType());

            // TODO: Evaluation at time
            if (timerRuleVO.getMessageType().equals("EVALUATION_AT_TIME")) {
                sentry.addBreadCrumb("[TimerResource] reserve EVALUATION_AT_TIME type task");
                // TODO: Task를 예약
                boolean taskReserveResult = false;
                Map<String, Object> data = new HashMap<String, Object>();
                data.put("userId", Integer.parseInt(userId));
                data.put("applyId", Integer.parseInt(applyId));
                data.put("timerTaskId", timerTaskVO.getTimerTaskId());
                data.put("relatedUserId", Integer.parseInt(relatedUserId));

                if (durationMin > 0) {
                    taskReserveResult = TimerUtil.getInstance().addEvaluationAtTimeTask(data, toIntExact(durationMin));
                } else {
                    taskReserveResult = TimerUtil.getInstance().addEvaluationAtTimeTask(data, 1);
                }

                if (!taskReserveResult) {
                    throw new Exception("Task is not reserved");
                }

                timerInfo.setCode(StatusCode.CREATED_SUCCESS);
                timerInfo.setTimerResult(true);
                return new Result<>(timerInfo);
            }

            if (timerRuleVO.getMessageType().equals("ATA") || timerRuleVO.getMessageType().equals("LMS") || timerRuleVO.getMessageType().equals(null) || timerRuleVO.getMessageType() == null) {
                sentry.addBreadCrumb("[TimerResource] reserve ATA OR LMS type task");
                boolean taskReserveResult = false;
                if (durationMin > 0) {
                    taskReserveResult = TimerUtil.getInstance().addTimerTask(timerTaskVO.getTimerTaskId(), toIntExact(durationMin));
                } else {
                    taskReserveResult= TimerUtil.getInstance().addTimerTask(timerTaskVO.getTimerTaskId(), 1);
                }

                if (!taskReserveResult) {
                    throw new Exception("Task is not reserved");
                }

                timerInfo.setCode(StatusCode.CREATED_SUCCESS);
                timerInfo.setTimerResult(true);
                return new Result<>(timerInfo);
            }

            throw new Exception("In valid Request");
        } catch (Exception e) {
            logger.error(e.getMessage());
            sentry.capture(e);
            timerInfo.setCode(StatusCode.API_SERVER_ERROR);
            timerInfo.setTimerResult(false);
            return new Result<>(timerInfo);
        }
    }

    public Result patch() {
        return success(202);
    }

    public Result delete() {
        return success(203);
    }
}
