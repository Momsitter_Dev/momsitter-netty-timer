package com.momsitter.demo;

import com.momsitter.nettyrest.netty.NettyRestServer;
import com.momsitter.nettyrest.utils.*;
import java.io.IOException;
import com.momsitter.schedules.MomsitterScheduler;
import com.momsitter.sentry.SentryLogger;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerLauncher {
    private static final Logger logger = LoggerFactory.getLogger(ServerLauncher.class);

    public static void main(String[] args) throws IOException, SchedulerException {
        SentryLogger sentry = new SentryLogger();
        sentry.addBreadCrumb("[System] start");
        sentry.addBreadCrumb("[System] database initialize");
        DatabaseInitializer.getInstance();

        sentry.addBreadCrumb("[System] HashedWheelTimer initialize");
        TimerUtil.getInstance();

        sentry.addBreadCrumb("[System] scheduler initialize");
        //new MomsitterScheduler();

        // TODO: 영속성 유지를 위해 서버를 다시 올렸을 경우 기존 이벤트들을 다시 타이머에 할당하는 로직 필요.
        // TODO Initialize Timer DB to HWT object
        sentry.addBreadCrumb("[System] loadPreviousTimerTask initialize");
        new LoadPreviousTimerTasks();

        sentry.addBreadCrumb("[System] http server start");
        NettyRestServer nettyRestServer = new NettyRestServer();
        nettyRestServer.start();
    }
}