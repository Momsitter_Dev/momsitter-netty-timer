package com.momsitter.demo.bo;

import com.momsitter.nettyrest.response.Info;

/**
 * Created by seungjaeyuk on 2017. 8. 28..
 */
public class TimerInfo extends Info {
    private String timerType;
    private boolean timerResult;

    public String getTimerType() {
        return timerType;
    }

    public void setTimerType(String timerType) {
        this.timerType = timerType;
    }

    public boolean getTimerResult() { return timerResult; }
    public void setTimerResult(boolean timerResult) { this.timerResult = timerResult; }
}
