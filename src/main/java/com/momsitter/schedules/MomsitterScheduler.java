package com.momsitter.schedules;

import com.momsitter.nettyrest.jobs.*;
import com.momsitter.sentry.SentryLogger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

public class MomsitterScheduler {
    private static final Logger logger = LoggerFactory.getLogger(MomsitterScheduler.class);
    private SchedulerFactory schedulerFactory;
    private Scheduler scheduler;
    private SentryLogger sentry = new SentryLogger();

    public MomsitterScheduler() {
        try {
            sentry.addBreadCrumb("[MomsitterScheduler]");
            schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.getScheduler();
            scheduler.start();

            sentry.addBreadCrumb("[MomsitterScheduler] job define");

            JobDetail resetApplyCountJob = newJob(RestUserApplyCountJob.class)
                    .withIdentity("REST_APPLY_COUNT")
                    .build();

            // TODO: 쿠폰 기간 만료
            // 쿠폰 기간 만료 3일전 저녘 9시반
            JobDetail couponExpireJob = newJob(CouponExpireNotificationJob.class)
                    .withIdentity("COUPON_EXPIRE")
                    .build();

            // TODO : 시작 응원 쿠폰
            JobDetail firstPaymentEncourage = newJob(FirstPaymentEncourageJob.class)
                    .withIdentity("FIRST_PAYMENT_ENCOURAGE")
                    .build();

            // TODO : 고객 감사 쿠폰
            JobDetail productCoupon = newJob(UserProductRemindJob.class)
                    .withIdentity("PRODUCT_REMIND")
                    .build();

            sentry.addBreadCrumb("[MomsitterScheduler] job trigger point setting");


            Trigger resetApplyCountTrigger = newTrigger()
                    .withIdentity("T_RESET_APPLY_COUNT")
                    .startNow()
                    .withSchedule(cronSchedule("59 59 11 * * ?"))
                    .build();


            Trigger couponExpireJobTrigger = newTrigger()
                    .withIdentity("COUPON_EXPIRE")
                    .startNow()
                    .withSchedule(cronSchedule("00 30 21 * * ?"))
                    .build();

            Trigger firstPaymentEncourageTrigger = newTrigger()
                    .withIdentity("FIRST_PAYMENT_ENCOURAGE")
                    .startNow()
                    .withSchedule(cronSchedule("00 30 20 * * ?"))
                    .build();

            Trigger productCouponTrigger = newTrigger()
                    .withIdentity("PRODUCT_REMIND")
                    .startNow()
                    .withSchedule(cronSchedule("00 00 21 * * ?"))
                    .build();


            sentry.addBreadCrumb("[MomsitterScheduler] job scheduling");

            scheduler.scheduleJob(resetApplyCountJob, resetApplyCountTrigger);
            scheduler.scheduleJob(couponExpireJob, couponExpireJobTrigger);
            scheduler.scheduleJob(productCoupon, productCouponTrigger);
            scheduler.scheduleJob(firstPaymentEncourage, firstPaymentEncourageTrigger);

            sentry.addBreadCrumb("[MomsitterScheduler] done");
        } catch (Exception e) {
            e.printStackTrace();
            sentry.capture(e);
            logger.error(e.getMessage());
        }
    }
}
