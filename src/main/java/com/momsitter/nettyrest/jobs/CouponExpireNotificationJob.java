package com.momsitter.nettyrest.jobs;

import com.momsitter.models.dao.*;
import com.momsitter.models.vo.ExpireCouponVO;
import com.momsitter.models.vo.LogBatchJobVO;
import com.momsitter.nettyrest.utils.LinkToken;
import com.momsitter.nettyrest.utils.MessageHistoryManager;
import com.momsitter.nettyrest.utils.SendLMSMessage;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

public class CouponExpireNotificationJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(CouponExpireNotificationJob.class);
    private final String COUPON_PAGE_LINK = "/my/coupon";
    private String getMessage (String link) {
        return  "보유하신 맘시터 쿠폰이 3일 후에 소멸됩니다!\n" +
                "내 쿠폰함 보기\n" +
                link + "\n" +
                "믿을 수 있는 든든한 맘시터가\n" +
                "아이를 즐겁고 안전하게 돌봐줍니다.";
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<ExpireCouponVO> coupons = new CronDAO().selectExpireCoupon();

        LogBatchJobVO log = new LogBatchJobVO();
        log.setStartTime(new Date().toString());
        log.setJobType("couponExpire");

        logger.info("COUPON LENGTH " + coupons.size());

        if (coupons == null || coupons.size() == 0) {
            // TODO: logging in batch
            log.setResult("success");
            log.setEndTime(new Date().toString());
            log.setMessage("target is empty");
            new LogBatchJobDAO().insertLogBatchJob(log);
            return;
        }

        int success = 0;
        int fail = 0;

        for (int i = 0; i < coupons.size(); i += 1) {
            MessageHistoryManager messageLog = new MessageHistoryManager();
            try {
                ExpireCouponVO current = coupons.get(i);
                if (current == null) {
                    fail += 1;
                    continue;
                }
                Integer targetUserId = current.getUserId();
                String targetPhoneNumber = current.getUserPhone();
                if (targetPhoneNumber == null) {
                    fail += 1;
                    continue;
                }


                String targetLink = new LinkToken().getLink(COUPON_PAGE_LINK, targetUserId);
                String message = getMessage(targetLink);

                boolean result = SendLMSMessage.getInstance().sendMessage(targetPhoneNumber, URLEncoder.encode(message, "UTF-8"));
                if (!result) {
                    fail += 1;
                    continue;
                }

                success += 1;

                // NOTE: Setting log data & save it
                messageLog.setSender(0);
                messageLog.setMsgType("BATCH");
                messageLog.setSenderInfo("couponExpire");
                messageLog.setReceiver(targetUserId);
                messageLog.setTo(targetPhoneNumber);
                messageLog.setMsg(message);
                messageLog.save();

            } catch (Exception e) {
                e.printStackTrace();
                fail += 1;
            }
        }

        log.setEndTime(new Date().toString());
        log.setResult("success");
        log.setMessage("target row count : " + coupons.size() + " // success : " + success + " // fail : " + fail);

        new LogBatchJobDAO().insertLogBatchJob(log);
    }
}
