package com.momsitter.nettyrest.jobs;

import com.momsitter.demo.ServerLauncher;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(SimpleJob.class);
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("momsitter:SimpleJob : START");
        logger.info("momsitter:SimpleJob : Job job");
        logger.info("momsitter:SimpleJob : END");
    }
}
