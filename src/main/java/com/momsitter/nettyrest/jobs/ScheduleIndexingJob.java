package com.momsitter.nettyrest.jobs;

import com.momsitter.models.dao.UserScheduleDAO;
import com.momsitter.models.dao.UserWeekdayTimeIndexDAO;
import com.momsitter.models.vo.UserIdVO;
import com.momsitter.models.vo.UserScheduleVO;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.SimpleDateFormat;
import java.util.*;

public class ScheduleIndexingJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(ScheduleIndexingJob.class);
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("momsitter:ScheduleIndexingJob => execute : START");


        String [] weekDayLabel = {"sun", "mon", "tue", "wed", "thu", "fri", "sat"};
        List<UserIdVO> users = new UserScheduleDAO().selectUserIds();
        /* TEST
        List<UserIdVO> users = new ArrayList<>();
        UserIdVO user = new UserIdVO();
        user.setUserId(24802);
        users.add(user);
        */
        UserScheduleVO currentSchedule = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        if (users != null && users.size() > 0) {
            for (Integer i = 0; i < users.size(); i += 1) {
                UserIdVO currentUser = users.get(i);
                List<UserScheduleVO> currentUserSchedule = new UserScheduleDAO().selectUserScheduleByUserId(currentUser.getUserId());
                Map<String, Object> currentUserIndexing = null;
                Date startDate = null;
                Date endDate = null;
                Date tempDate = null;
                Integer startTime = 0;
                Integer endTime = 0;
                Integer weekDay = 0;

                if (currentUserSchedule != null && currentUserSchedule.size() > 0) {
                    currentUserIndexing = new HashMap<String, Object>();

                    try {
                        for (Integer k = 0; k < currentUserSchedule.size(); k += 1) {
                            currentSchedule = currentUserSchedule.get(k);
                            tempDate = dateFormat.parse(currentSchedule.getScheduleDate());
                            if (k.equals(0)) {
                                startDate = tempDate;
                                endDate = tempDate;
                            } else {
                                if (startDate.compareTo(tempDate) > 0) {
                                    startDate = tempDate;
                                }
                                if (endDate.compareTo(tempDate) < 0) {
                                    endDate = tempDate;
                                }
                            }

                            weekDay = tempDate.getDay();

                            startTime = timeFormat.parse(currentSchedule.getScheduleStartTime()).getHours();
                            endTime = timeFormat.parse(currentSchedule.getScheduleEndTime()).getHours();

                            currentUserIndexing.put(weekDayLabel[weekDay], true);

                            if ((startTime >= 7 && startTime <= 12) || (endTime >= 7 && endTime <= 12)) {
                                currentUserIndexing.put(weekDayLabel[weekDay] + "Morning", true);
                            } else if ((startTime >= 12 && startTime <= 18) || (endTime >= 12 && endTime <= 18)) {
                                currentUserIndexing.put(weekDayLabel[weekDay] + "Noon", true);
                            } else {
                                currentUserIndexing.put(weekDayLabel[weekDay] + "Night", true);
                            }
                        }
                        currentUserIndexing.put("userId", currentUser.getUserId());
                        currentUserIndexing.put("startDate", dateFormat.format(startDate));
                        currentUserIndexing.put("endDate", dateFormat.format(endDate));

                        logger.info("momsitter:ScheduleIndexingJob : DELETE USER INDEX : " + currentUser.getUserId());
                        // TODO: DELETE INDEX BY USER
                        new UserWeekdayTimeIndexDAO().deleteIndexByUserId(currentUser.getUserId());
                        logger.info("momsitter:ScheduleIndexingJob : DELETE USER INDEX : " + currentUser.getUserId() + " : DONE");

                        // TODO: INSERT INDEX
                        logger.info("momsitter:ScheduleIndexingJob : INSERT USER INDEX : " + currentUser.getUserId());
                        new UserWeekdayTimeIndexDAO().insertIndex(currentUserIndexing);
                        logger.info("momsitter:ScheduleIndexingJob : INSERT USER INDEX : " + currentUser.getUserId() + " : DONE");
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.info("momsitter:ScheduleIndexingJob => execute : USER ONE BY ONE : " + i + " : START");
                    } finally {
                        continue;
                    }
                } else {
                    // TODO: ERROR
                    logger.info("momsitter:ScheduleIndexingJob => execute : ERROR : USER NOT EXIST");
                }
            }
        } else {
            // TODO: ERROR
            logger.info("momsitter:ScheduleIndexingJob => execute : ERROR : USER NOT EXIST");
        }

        logger.info("momsitter:ScheduleIndexingJob => execute : END");
    }
}
