package com.momsitter.nettyrest.jobs;

import com.momsitter.models.dao.LogBatchJobDAO;
import com.momsitter.models.dao.UserDAO;
import com.momsitter.models.vo.LogBatchJobVO;
import com.momsitter.models.vo.RemindSitters;
import net.nurigo.java_sdk.api.GroupMessage;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

public class SitterRemindNotificationJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(SitterRemindNotificationJob.class);
    private static final String MESSAGE = "[맘시터] 지원횟수 1회 충전완료!\n" +
            "내게 꼭 맞는 일자리에 지원해보세요.\n" +
            "www.mom-sitter.com";

    public boolean sendLMSMessage(String to) {
        boolean result = false;
        logger.info("SEND LMS MESSAGE : START");
        try {
            String url = "http://rest.supersms.co:6200/sms/xml?" +
                    "id=momsitter" +
                    "&pwd=QKOUK67786LBO78MIXFH" +
                    "&from=18336331" +
                    "&to_country=82" +
                    "&to=" + to +
                    "&report_req=0" +
                    "&title=title" +
                    "&message=" + URLEncoder.encode(MESSAGE, "UTF-8");

            logger.info("DEBUG : URI : " + url);

            HttpClient httpClient = new DefaultHttpClient();
            HttpRequestBase httpGet = new HttpGet(url);
            HttpResponse response = httpClient.execute(httpGet);

            // TODO RESPONSE PARSING
            logger.info(response.toString());
            if (response.getStatusLine().getStatusCode() == 200) {
                logger.info("MESSAGE SENDED");
                HttpEntity entity = response.getEntity();
                // TODO: Parsing response document as XML
                int size = 0;
                byte [] buffer = new byte[1024];
                while((size = entity.getContent().read(buffer)) != -1) {
                    System.out.write(buffer, 0, size);
                }
                /*
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    Document document = builder.parse(entity.getContent());
                    document.get
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.toString());
                }
                */
                result = true;
            } else {
                logger.info("MESSAGE SEND FAIL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("SEND LMS MESSAGE : END : " + result);
        return result;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<RemindSitters> sitters = new UserDAO().selectRemindSitter();
        if (sitters == null) {
            logger.info("sitter is null");
        }

        if (sitters.size() == 0) {
            logger.info("sitter is null 2");
        }

        int successCount = 0;
        int failCount = 0;
        for (int i = 0; i < sitters.size(); i += 1) {
            logger.info("sendmessage" + i + " // " + sitters.get(i).getUserPhone());
            boolean result = this.sendLMSMessage(sitters.get(i).getUserPhone());
            if (result) {
                successCount += 1;
            } else {
                failCount += 1;
            }
        }

        try {
            LogBatchJobVO log = new LogBatchJobVO();
            log.setJobType("sitterApplyCountRemind");
            log.setMessage("success " + successCount + " // " + "fail " + failCount);
            log.setResult("success");
            new LogBatchJobDAO().insertLogBatchJob(log);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
