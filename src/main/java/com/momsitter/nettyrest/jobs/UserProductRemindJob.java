package com.momsitter.nettyrest.jobs;

import com.momsitter.models.dao.CronDAO;
import com.momsitter.models.dao.LogBatchJobDAO;
import com.momsitter.models.vo.CouponVO;
import com.momsitter.models.vo.LogBatchJobVO;
import com.momsitter.models.vo.UserProductRemindVO;
import com.momsitter.nettyrest.utils.LinkToken;
import com.momsitter.nettyrest.utils.MessageHistoryManager;
import com.momsitter.nettyrest.utils.SendLMSMessage;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UserProductRemindJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(UserProductRemindJob.class);
    private final String COUPON_PAGE_LINK = "/my/coupon";
    private String getMessage (String link, String userName) {
        return  userName + "님, 오늘도 아이 키우느라 고생하셨습니다.\n" +
                "\n" +
                "하루종일 눈코뜰새 없는 당신을 돕기 위해, 고객감사쿠폰 5,000원을 선물드립니다.\n" +
                "\n" +
                "언제나 준비되어 있는 맘시터를 다시 시작해보세요.\n" +
                "\n" +
                "내 쿠폰함 보기\n" +
                link + "\n" +
                "믿을 수 있는 든든한 맘시터가\n" +
                "아이를 즐겁고 안전하게 돌봐줍니다.\n" +
                "대한민국 No.1 아이돌봄, 맘시터";
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<UserProductRemindVO> products = new CronDAO().selectUserProductRemind();

        LogBatchJobVO log = new LogBatchJobVO();
        log.setStartTime(new Date().toString());
        log.setJobType("productRemind");

        logger.info("COUPON LENGTH " + products.size());

        if (products == null || products.size() == 0) {
            // TODO: logging in batch
            log.setResult("success");
            log.setEndTime(new Date().toString());
            log.setMessage("target is empty");
            new LogBatchJobDAO().insertLogBatchJob(log);
            return;
        }

        int success = 0;
        int fail = 0;

        for (int i = 0; i < products.size(); i += 1) {
            MessageHistoryManager messageLog = new MessageHistoryManager();
            try {
                UserProductRemindVO current = products.get(i);
                if (current == null) {
                    fail += 1;
                    continue;
                }
                String targetPhoneNumber = current.getUserPhone();
                String targetUserName = current.getUserName();
                Integer targetUserId = current.getUserId();
                if (targetPhoneNumber == null) {
                    fail += 1;
                    continue;
                }

                CouponVO coupon = new CouponVO();
                coupon.setUserId(targetUserId);
                coupon.setCouponTypeId(3); // TODO Constant coupon type
                coupon.setStatus("available");
                coupon.setRead(0);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, 30);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                coupon.setExpireDate(dateFormat.format(calendar.getTime())); // TODO Date + 30

                Integer couponCreateResult = new CronDAO().insertNewCoupon(coupon);

                if (couponCreateResult == null) {
                    fail += 1;
                    continue;
                }

                String targetLink = new LinkToken().getLink(COUPON_PAGE_LINK + "?new=" + couponCreateResult, targetUserId);
                String message = getMessage(targetLink, targetUserName);

                boolean result = SendLMSMessage.getInstance().sendMessage(targetPhoneNumber, URLEncoder.encode(message, "UTF-8"));
                if (!result) {
                    fail += 1;
                    continue;
                }

                success += 1;

                // NOTE: Setting log data & save it
                messageLog.setSender(0);
                messageLog.setMsgType("BATCH");
                messageLog.setSenderInfo("productRemind");
                messageLog.setReceiver(targetUserId);
                messageLog.setTo(targetPhoneNumber);
                messageLog.setMsg(message);
                messageLog.save();

            } catch (Exception e) {
                e.printStackTrace();
                fail += 1;
            }
        }

        log.setEndTime(new Date().toString());
        log.setResult("success");
        log.setMessage("target row count : " + products.size() + " // success : " + success + " // fail : " + fail);

        new LogBatchJobDAO().insertLogBatchJob(log);
    }
}
