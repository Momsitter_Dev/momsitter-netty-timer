package com.momsitter.nettyrest.jobs;

import com.momsitter.models.dao.CronDAO;
import com.momsitter.models.dao.LogBatchJobDAO;
import com.momsitter.models.vo.CouponVO;
import com.momsitter.models.vo.FirstPaymentEncourageVO;
import com.momsitter.models.vo.LogBatchJobVO;
import com.momsitter.nettyrest.utils.LinkToken;
import com.momsitter.nettyrest.utils.MessageHistoryManager;
import com.momsitter.nettyrest.utils.SendLMSMessage;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FirstPaymentEncourageJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(FirstPaymentEncourageJob.class);
    private final String COUPON_PAGE_LINK = "/my/coupon";
    private String getMessage (String link, String userName) {
        return "맘시터 시작이 망설여지시나요?\n"
                + "\n"
                + userName + "님이 부담없이 맘시터를 시작할 수 있도록, 시작응원쿠폰 5,000원을 발급해드렸습니다.\n"
                + "\n"
                + "내 쿠폰함 보기\n"
                + link + "\n"
                + " \n"
                + "믿을 수 있는 든든한 맘시터가\n"
                + "아이를 즐겁고 안전하게 돌봐줍니다.\n"
                + "\n"
                + "대한민국 No.1 아이돌봄, 맘시터";
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<FirstPaymentEncourageVO> targets = new CronDAO().selectFirstPaymentEncourageTarget();

        LogBatchJobVO log = new LogBatchJobVO();
        log.setStartTime(new Date().toString());
        log.setJobType("firstPaymentEncourage");

        logger.info("COUPON LENGTH " + targets.size());

        if (targets == null || targets.size() == 0) {
            // TODO: logging in batchf
            log.setResult("success");
            log.setEndTime(new Date().toString());
            log.setMessage("target is empty");
            new LogBatchJobDAO().insertLogBatchJob(log);
            return;
        }

        int success = 0;
        int fail = 0;

        for (int i = 0; i < targets.size(); i += 1) {
            MessageHistoryManager messageLog = new MessageHistoryManager();
            try {
                FirstPaymentEncourageVO current = targets.get(i);
                if (current == null) {
                    fail += 1;
                    continue;
                }
                String targetPhoneNumber = current.getUserPhone();
                String targetUserName = current.getUserName();
                Integer targetUserId = current.getUserId();
                if (targetPhoneNumber == null) {
                    fail += 1;
                    continue;
                }

                // TODO: create new Coupon
                CouponVO coupon = new CouponVO();
                coupon.setUserId(targetUserId);
                coupon.setCouponTypeId(5); // TODO Constant coupon type
                coupon.setStatus("available");
                coupon.setRead(0);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, 30);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                coupon.setExpireDate(dateFormat.format(calendar.getTime())); // TODO Date + 30

                Integer couponCreateResult = new CronDAO().insertNewCoupon(coupon);

                if (couponCreateResult == null) {
                    fail += 1;
                    continue;
                }

                String targetLink = new LinkToken().getLink(COUPON_PAGE_LINK + "?new=" + couponCreateResult, targetUserId);
                String message = getMessage(targetLink, targetUserName);
                boolean result = SendLMSMessage.getInstance().sendMessage(targetPhoneNumber, URLEncoder.encode(message, "UTF-8"));
                if (!result) {
                    fail += 1;
                    continue;
                }

                success += 1;

                // NOTE: Setting log data & save it
                messageLog.setSender(0);
                messageLog.setMsgType("BATCH");
                messageLog.setSenderInfo("firstPaymentEncourage");
                messageLog.setReceiver(targetUserId);
                messageLog.setTo(targetPhoneNumber);
                messageLog.setMsg(message);
                messageLog.save();

            } catch (Exception e) {
                e.printStackTrace();
                fail += 1;
            }
        }

        log.setEndTime(new Date().toString());
        log.setResult("success");
        log.setMessage("target row count : " + targets.size() + " // success : " + success + " // fail : " + fail);

        new LogBatchJobDAO().insertLogBatchJob(log);
    }
}
