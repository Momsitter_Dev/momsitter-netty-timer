package com.momsitter.nettyrest.jobs;

import com.momsitter.models.dao.ProductDAO;
import com.momsitter.models.dao.UserApplyCountDAO;
import com.momsitter.models.dao.UserProductDAO;
import com.momsitter.models.vo.ProductVO;
import com.momsitter.models.vo.UserProductVO;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestUserApplyCountJob implements Job {
    private static final Logger logger = LoggerFactory.getLogger(RestUserApplyCountJob.class);
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("momsitter:ResetUserApplyCountJob => execute : START");
        List<ProductVO> products = new ProductDAO().getAllProduct();
        List<UserProductVO> userProducts = new UserProductDAO().selectNonExpiredUserProducts();

        // NOTE: product list => Map
        Map<Integer, ProductVO> productMap = new HashMap<Integer, ProductVO>();
        for (ProductVO product : products) {
            productMap.put(product.getProductId(), product);
        }

        // TODO: Rest Count as [0, 0]
        new UserApplyCountDAO().updateCountAsZero();

        // TODO: Reset Count user one by one
        UserProductVO currentUserProduct = null;
        ProductVO targetProduct = null;
        for (Integer i = 0; i < userProducts.size(); i += 1) {
            currentUserProduct = userProducts.get(i);
            if (currentUserProduct != null) {
                targetProduct = productMap.get(currentUserProduct.getProductId());
                if (targetProduct != null) {
                    new UserApplyCountDAO().updateCountByUserId(currentUserProduct.getUserId(), targetProduct.getProductApplyToSitterCount(), targetProduct.getProductApplyToParentCount());
                } else {
                    // TODO: ERROR : TargetProduct is null
                    logger.info("momsitter:ResetUserApplyCountJob => execute : UPDATE USER ONE BY ONE : TargetProduct is Null=" + i);
                }
            } else {
                // TODO: ERROR : UserProduct is null
                logger.info("momsitter:ResetUserApplyCountJob => execute : UPDATE USER ONE BY ONE : UserProduct is NullL" + i);
            }
        }
        logger.info("momsitter:ResetUserApplyCountJob => execute : UPDATE USER ONE BY ONE : DONE");
    }
}
