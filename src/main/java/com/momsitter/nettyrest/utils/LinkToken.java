package com.momsitter.nettyrest.utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LinkToken {
    private static final Logger logger = LoggerFactory.getLogger(LinkToken.class);
    /*http://13.124.133.153/
    private final String TOKEN_GENERATE_PATH = "http://13.124.133.153:5000/api/linktoken";
    private final String KEY = "A66D1C13AF96CFE5AAD893E16810596A19FA0F41EF9B1C070E266193CB4D8DFA";
    private final String HANDLE_TOKEN_LINK = "http://13.124.133.153:3000/handleTokenLogin";
    */

    private final String TOKEN_GENERATE_PATH = "https://api.mom-sitter.com/api/linktoken";
    private final String KEY = "A66D1C13AF96CFE5AAD893E16810596A19FA0F41EF9B1C070E266193CB4D8DFA";
    private final String HANDLE_TOKEN_LINK = "https://www.mom-sitter.com/handleTokenLogin";

    /**
     *
     * @param url
     * @return String url
     */
    public String getLink(String url, Integer userId) {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(TOKEN_GENERATE_PATH);

            StringEntity requestBody = new StringEntity("{ userId: " + userId + ", link: " + url + " }");

            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("userId", userId.toString()));
            param.add(new BasicNameValuePair("link", url));

            httpPost.setEntity(new UrlEncodedFormEntity(param));

            httpPost.addHeader("x-api-key", KEY);

            HttpResponse response = httpClient.execute(httpPost);
            logger.info("GET RESPONSE");
            if (response.getStatusLine().getStatusCode() == 200) {
                logger.info("GET RESPONSE : STATUS : 200");
                String jsonString = EntityUtils.toString(response.getEntity());
                JSONObject responseJson = new JSONObject(jsonString);
                logger.info("GET RESPONSE : STATUS : 200 : " + jsonString);
                return HANDLE_TOKEN_LINK + "?t=" +responseJson.get("linkToken");
            }
            logger.info("LINK TOKEN ERROR");
            throw new Exception("Cannot create link token");
        } catch (Exception e) {
            e.printStackTrace();
            // 실패할 경우 원본 링크를 리턴한다
            return url;
        }
    }
}
