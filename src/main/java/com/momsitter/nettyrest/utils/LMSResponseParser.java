package com.momsitter.nettyrest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class LMSResponseParser {
    private static final Logger logger = LoggerFactory.getLogger(LMSResponseParser.class);
    private static LMSResponseParser instance;
    private LMSResponseParser () {
        logger.info("Timer Server Initialize : setInstance");
    }

    public static LMSResponseParser getInstance () {
        if (instance == null) {
            instance = new LMSResponseParser();
        }
        return instance;
    }

    public boolean isSuccess(String xml) {
        boolean result = false;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is;
        try {
            builder = factory.newDocumentBuilder();
            is = new InputSource(new StringReader(xml));
            Document doc = builder.parse(is);
            NodeList list = doc.getElementsByTagName("err_code");
            logger.info("DEBUG : RESPONSE : " + list.item(0).getTextContent());
            String responseCode = list.item(0).getTextContent();
            if (responseCode != null) {
                if (responseCode.equals("R000")) {
                    result = true;
                }
            }
        } catch (ParserConfigurationException e) {
            logger.error(e.toString());
        } catch (SAXException e) {
            logger.error(e.toString());
        } catch (IOException e) {
            logger.error(e.toString());
        }

        return result;
    }
}
