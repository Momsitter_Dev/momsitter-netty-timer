package com.momsitter.nettyrest.utils;

import com.momsitter.models.dao.UserDAO;
import com.momsitter.models.vo.TimerTaskTargetUserVO;
import com.momsitter.nettyrest.utils.timers.SimpleMessageSender;
import com.momsitter.sentry.SentryLogger;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class AfterRoutine {
    private static final Logger logger = LoggerFactory.getLogger(SimpleMessageSender.class);
    private static final String PROFILE_INACTIVE = "inactive";
    private SentryLogger sentry = new SentryLogger();

    public boolean syncUser(Integer userId) {
        try {
            String url = "https://api.mom-sitter.com/api/sync/" + userId;
            HttpClient httpClient = new DefaultHttpClient();
            HttpRequestBase httpPut = new HttpPut(url);
            httpPut.addHeader("Content-Type", "application/json");
            httpPut.addHeader("Accept", "application/json");

            JSONObject body = new JSONObject();
            body.put("key", "D0F43E39AC923B74F805E19B7109084DF3D71662E4299B6465EEF97435F9EE54");


            ((HttpPut) httpPut).setEntity(new StringEntity(body.toString()));
            sentry.addBreadCrumb("[SimpleMessageSender] sync ... " + body.toString());
            httpClient.execute(httpPut);

            return true;
        } catch (Exception e) {
            sentry.capture(e);
            return false;
        }
    }

    public void execute(Integer timerRuleId, TimerTaskTargetUserVO data) {
        sentry.addBreadCrumb("[SimpleMessageSender] start");
        try {
            if (timerRuleId == null) {
                throw new Error("[SimpleMessageSender] TimerRuleId is null");
            }

            if (data == null) {
                throw new Error("[SimpleMessageSender] data is null");
            }

            if (timerRuleId.equals(3)) {
                // 프로필 비활성화
                Map param = new HashMap<String, Object>();
                param.put("userId", data.getUserId());
                param.put("profileStatus", PROFILE_INACTIVE);
                new UserDAO().updateParentProfileStatus(param);
                if (!this.syncUser(data.getUserId())) {
                    throw new Error("[SimpleMessageSender] Sync is not working");
                }
            }
            if (timerRuleId.equals(17)) {
                // 프로필 비활성화
                Map param = new HashMap<String, Object>();
                param.put("userId", data.getUserId());
                param.put("profileStatus", PROFILE_INACTIVE);
                new UserDAO().updateSitterProfileStatus(param);
                if (!this.syncUser(data.getUserId())) {
                    throw new Error("[SimpleMessageSender] Sync is not working");
                }
            }
        } catch (Exception e) {
            sentry.capture(e);
            e.printStackTrace();
        }

    }
}
