package com.momsitter.nettyrest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;

public class MessageUtil {
    private static final Logger logger = LoggerFactory.getLogger(MessageUtil.class);
    private static MessageUtil instance;
    private HashMap<String, HashMap<String, String>> ataMessageMap;
    private HashMap<String, HashMap<String, String>> lmsMessageMap;

    private final String MESSAGE_TYPE_ATA = "ATA";
    private final String MESSAGE_TYPE_LMS = "LMS";

    private MessageUtil () {
        logger.info("MessageUtil Initialize : setInstance");
        ataMessageMap = new HashMap<>();
        lmsMessageMap = new HashMap<>();
        //init();
    }

    public static MessageUtil getInstance () {
        if (instance == null) {
            instance = new MessageUtil();
        }
        return instance;
    }

    /*
    public HashMap<String, String> getATATemplateInfo(String templateCode) {
        return ataMessageMap.get(templateCode);
    }

    public HashMap<String, String> getLMSTemplateInfo(String messageName) {
        return lmsMessageMap.get(messageName);
    }


    public String getTagValue (String tagName, Element element) {
        String value = null;
        NodeList nodeList = element.getElementsByTagName(tagName);
        if (nodeList != null) {
            if (nodeList.item(0) != null) {
                nodeList = nodeList.item(0).getChildNodes();
                value = nodeList.item(0).getNodeValue();
            }
        }
        return value;
    }

    public void init() {
        File xmlFolder = new File(getClass().getClassLoader().getResource("messages").getFile());
        if (xmlFolder.isDirectory()) {
            File[] xmlFiles = xmlFolder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".xml");
                }
            });
            Integer fileCount = xmlFiles.length;
            if (fileCount > 0) {
                for (Integer i = 0; i < fileCount; i += 1) {
                    logger.info(i + "===========================================");
                    File xmlFile = xmlFiles[i];
                    if (xmlFile.exists()) {
                        logger.info("PARSING XML START");
                        try {
                            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                            Document doc = documentBuilder.parse(xmlFile);
                            doc.getDocumentElement().normalize();
                            logger.info("Root element :" + doc.getDocumentElement().getNodeName());

                            String rootNodeName = doc.getDocumentElement().getNodeName();
                            NodeList nList = doc.getElementsByTagName(rootNodeName);

                            logger.info("-------------------------------------------------------");
                            for (int k = 0; k < nList.getLength(); k += 1 ) {
                                Node node = nList.item(k);
                                if (node.getNodeType() == Node.ELEMENT_NODE) {
                                    Element element = (Element) node;
                                    String messageType = this.getTagValue("messageType", element);

                                    if (messageType.equals(MESSAGE_TYPE_ATA)) {
                                        // NOTE: ATA방식일 경우에는 templateCode가 Key값이 된다.
                                        String templateCode = this.getTagValue("templateCode", element);
                                        String buttonLink = this.getTagValue("buttonLink", element);
                                        String buttonButton = this.getTagValue("buttonButton", element);
                                        String template = this.getTagValue("template", element);

                                        HashMap templateInfo = new HashMap<String, String>();
                                        templateInfo.put("buttonLink", buttonLink);
                                        templateInfo.put("buttonButton", buttonButton);
                                        templateInfo.put("template", template);

                                        ataMessageMap.put(templateCode, templateInfo);
                                    } else if (messageType.equals(MESSAGE_TYPE_LMS)) {
                                        // NOTE: LMS 방식일 경우
                                        String messageName = this.getTagValue("messageName", element);
                                        String template = this.getTagValue("template", element);

                                        HashMap templateInfo = new HashMap<String, String>();
                                        templateInfo.put("messageName", messageName);
                                        templateInfo.put("template", template);

                                        lmsMessageMap.put(messageName, templateInfo);
                                    } else {
                                        // TODO: ERROR UNKNOWN MESSAGE TYPE
                                        logger.info("UNKNOWN MESSAGE TYPE");
                                    }
                                    logger.info("templateCode : " + this.getTagValue("templateCode", element));
                                    logger.info("buttonLink : " + this.getTagValue("buttonLink", element));
                                    logger.info("buttonName : " + this.getTagValue("buttonName", element));
                                    logger.info("template : " + this.getTagValue("template", element).trim());
                                }
                            }
                            logger.info("-------------------------------------------------------");
                        } catch (Exception e) {
                            e.printStackTrace();
                            logger.error(e.toString());
                        }
                    } else {
                        // TODO: ERROR TEMPLATE FILE NOT EXIST
                        logger.info("xml file not exist");
                    }
                    logger.info("==============================================");
                }
            } else {
                // TODO: ERROR TEMPLATE FILE NOT FOUND
            }
        } else {
            // TODO: ERROR TEMPLATE FOLDER NOT FOUND
            logger.info("DEBUG : 'message' folder not found");
        }
    }
    */
}
