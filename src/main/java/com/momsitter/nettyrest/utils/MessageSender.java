package com.momsitter.nettyrest.utils;

import net.nurigo.java_sdk.api.Message;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.dom4j.DocumentFactory;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;

public class MessageSender {
    private static final Logger logger = LoggerFactory.getLogger(MessageSender.class);
    private static MessageSender instance;
    private MessageSender () {
        logger.info("MessageSender Initialize : setInstance");
    }

    public static MessageSender getInstance () {
        if (instance == null) {
            instance = new MessageSender();
        }
        return instance;
    }

    public String encode(String value) {
        String result = value;
        try {
            result = URLEncoder.encode(result, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString());
        }
        return result;
    }

    public static String encodeURIComponent(String s) {
        String result = null;
        try {
            result = URLEncoder.encode(s, "UTF-8")
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
            result = s;
            result = result.replaceAll("(\\r|\\n|\\r\\n)+", "\\\\n");
            result = result.replaceAll("(\\t|\t)+", "");

        }
        return result;
    }

    public boolean sendLMSMessage(String message) {
        boolean result = false;
        logger.info("SEND LMS MESSAGE : START");
        try {
            String url = "http://rest.supersms.co:6200/sms/xml?" +
                    "id=momsitter" +
                    "&pwd=QKOUK67786LBO78MIXFH" +
                    "&from=18336331" +
                    "&to_country=82" +
                    "&to=01028505316" +
                    "&report_req=0" +
                    "&title=title" +
                    "&message=" + message;

            logger.info("DEBUG : URI : " + url);

            HttpClient httpClient = new DefaultHttpClient();
            HttpRequestBase httpGet = new HttpGet(url);
            HttpResponse response = httpClient.execute(httpGet);

            // TODO RESPONSE PARSING
            logger.info(response.toString());
            if (response.getStatusLine().getStatusCode() == 200) {
                logger.info("MESSAGE SENDED");
                HttpEntity entity = response.getEntity();
                // TODO: Parsing response document as XML
                int size = 0;
                byte [] buffer = new byte[1024];
                while((size = entity.getContent().read(buffer)) != -1) {
                    System.out.write(buffer, 0, size);
                }
                /*
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    Document document = builder.parse(entity.getContent());
                    document.get
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.toString());
                }
                */
                result = true;
            } else {
                logger.info("MESSAGE SEND FAIL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("SEND LMS MESSAGE : END : " + result);
        return result;
    }

    public boolean sendATAMessage() {
        boolean result = false;
        String apiKey       = "NCS58D23951764C8";
        String apiSecret    = "7973AFE810818883CC4F0348358B63EF";
        try {
            // TODO: 메세지를 하나씩 보내지 않고 한번에 보낼 수 있는 방법이 있을까?
            // REF: https://www.coolsms.co.kr/index.php?mid=JAVA_SDK_EXAMPLE_GroupMessage
            Message coolsms = new Message(apiKey, apiSecret);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("from", "18336331");
            params.put("type", "ATA");
            params.put("sender_key", "01bff383c2e18a9f171e8aa11cb6ac5f6f1f4012");
            params.put("app_version", "JAVA SDK v2.2");
            params.put("to", "01028505316");
            //params.put("text", MessageUtil.getInstance().getATATemplateInfo("SRemind01").get("template"));
            params.put("template_code", "SRemind01");

            JSONObject response = coolsms.send(params);
        } catch (Exception e) {
            //logger.debug(" MESSAGE : " + e.getMessage());
            //logger.debug(" MESSAGE CODE : " + e.getCode());
            e.printStackTrace();
        }


        return result;
    }
}