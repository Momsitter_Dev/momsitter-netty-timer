package com.momsitter.nettyrest.utils;

import com.momsitter.nettyrest.utils.timers.EvaluationAtTimeTask;
import com.momsitter.nettyrest.utils.timers.SimpleMessageSender;
import io.netty.util.HashedWheelTimer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by seungjaeyuk on 2017. 8. 27..
 */
public class TimerUtil {
    private static final Logger logger = LoggerFactory.getLogger(TimerUtil.class);
    private static TimerUtil instance;
    private static HashedWheelTimer timer;
    private TimerUtil () {
        logger.info("Timer Server Initialize : setInstance");
        timer = new HashedWheelTimer();
        timer.start();
    }

    public static TimerUtil getInstance () {
        if (instance == null) {
            instance = new TimerUtil();
        }
        return instance;
    }

    public boolean addTimerTask(int timerTaskId, Integer minutes) {
        boolean result = false;
        try {
            timer.newTimeout(new SimpleMessageSender(timerTaskId).init(), minutes, TimeUnit.MINUTES);
            result = true;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addEvaluationAtTimeTask(Map<String, Object> data, Integer minutes) {
        boolean result = false;
        try {
            timer.newTimeout(new EvaluationAtTimeTask(data).init(), minutes, TimeUnit.MINUTES);
            result = true;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean startTimer() {
        boolean result = false;
        try {
            timer.start();
        } catch(Exception e) {
            logger.error("momsitter:timer start Timer Fail");
        }
        return result;
    }

    public boolean stopTimer() {
        boolean result = false;
        try {
            // Releases all resources acquired by this Timer and cancels all tasks which were scheduled but not executed yet.
            // timer에 추가 되어있던 작업들을 모두 없에버리는 걸로보임
            timer.stop();
        } catch (Exception e) {
            logger.error("momsitter:timer stop Timer Fail");
        }
        return result;
    }
}
