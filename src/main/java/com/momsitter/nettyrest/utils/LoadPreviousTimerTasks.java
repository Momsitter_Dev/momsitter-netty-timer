package com.momsitter.nettyrest.utils;

import com.momsitter.models.dao.TimerTaskDAO;
import com.momsitter.models.vo.TimerTaskWithTargetVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.toIntExact;

public class LoadPreviousTimerTasks {
    private static final Logger logger = LoggerFactory.getLogger(LoadPreviousTimerTasks.class);

    public LoadPreviousTimerTasks() {
        logger.info("momsitter:LoadPreviousTimerTasks : START");
        List<TimerTaskWithTargetVO> timerTasks = new TimerTaskDAO().getPendingTimerTasks();
        if (timerTasks != null && timerTasks.size() > 0) {
            for (Integer i = 0; i < timerTasks.size(); i += 1) {
                try {
                    DateTimeFormatter formatter         = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime now                   = LocalDateTime.now();
                    String reservedDateStr              = timerTasks.get(i).getReservedDate();
                    LocalDateTime parsedReservedDate    = LocalDateTime.parse(reservedDateStr, formatter);
                    long durationMin = Duration.between(now, parsedReservedDate).toMinutes();

                    boolean isEvaluationAtTime = false;
                    Map<String, Object> evaluationData = new HashMap<String, Object>();

                    // Evaluation type
                    if (timerTasks.get(i).getTimerRuleId().equals(52) || timerTasks.get(i).getTimerRuleId().equals(53)|| timerTasks.get(i).getTimerRuleId().equals(58) || timerTasks.get(i).getTimerRuleId().equals(59)) {
                        isEvaluationAtTime = true;
                        evaluationData.put("userId", timerTasks.get(i).getUserId());
                        evaluationData.put("timerTaskId", timerTasks.get(i).getTimerTaskId());
                        evaluationData.put("relatedUserId", timerTasks.get(i).getRelatedUserId());
                    }

                    logger.info("momsitter:LoadPreviousTimerTasks => reservedDate to Min = " + durationMin);
                    if (durationMin > 0 ){
                        if (isEvaluationAtTime) {
                            TimerUtil.getInstance().addEvaluationAtTimeTask(evaluationData, toIntExact(durationMin));
                        } else {
                            TimerUtil.getInstance().addTimerTask(timerTasks.get(i).getTimerTaskId(), toIntExact(durationMin));
                        }
                    } else {
                        if (isEvaluationAtTime) {
                            TimerUtil.getInstance().addEvaluationAtTimeTask(evaluationData, 1);
                        } else {
                            TimerUtil.getInstance().addTimerTask(timerTasks.get(i).getTimerTaskId(), 1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info("momsitter:LoadPreviousTimerTasks : ERROR : TIMER TASK ID : " +  timerTasks.get(i).getTimerTaskId());
                }
            }
        } else {
            // ERROR
            logger.info("momsitter:LoadPreviousTimerTasks : ERROR");
        }
        logger.info("momsitter:LoadPreviousTimerTasks : END");
    }
}
