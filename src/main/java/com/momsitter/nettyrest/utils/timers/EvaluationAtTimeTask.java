package com.momsitter.nettyrest.utils.timers;

import com.momsitter.models.dao.MessageHistoryDAO;
import com.momsitter.models.dao.TimerRuleDAO;
import com.momsitter.models.dao.TimerTaskDAO;
import com.momsitter.models.dao.UserDAO;
import com.momsitter.models.vo.*;
import com.momsitter.nettyrest.utils.LinkToken;
import com.momsitter.nettyrest.utils.SendLMSMessage;
import com.momsitter.sentry.SentryLogger;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class EvaluationAtTimeTask {
    private static final Logger logger = LoggerFactory.getLogger(EvaluationAtTimeTask.class);
    private Map<String, Object> taskData;
    private final String USER_STATUS_ACTIVE = "active";
    private final String STATUS_DONE = "done";
    private final String STATUS_PROGRESS = "in-progress";
    private final String STATUS_PENDING = "pending";
    private final String STATUS_FAIL = "fail";
    private final String STATUS_CANCEL = "cancel";
    private final String STATUS_CANCEL_POLICY = "cancel-policy";
    private String REVIEW_WRITE_PAGE_URL = "/review/new";

    private UserVO user;
    private UserRoleVO userRole;
    private UserVO relatedUser;
    private TimerTaskVO timerTask;
    private TimerRuleVO timerRule;
    private int applyId;
    private MessageHistoryVO history;


    public EvaluationAtTimeTask(Map<String, Object> data) {
        this.taskData = data;
    }

    public TimerTask init() {
        return new TimerTask() {
            @Override
            public void run(Timeout timeout) throws Exception {
                SentryLogger sentry = new SentryLogger();
                sentry.addBreadCrumb("[EvaluationAtTimeTask] start");
                history = new MessageHistoryVO();
                try {
                    sentry.addBreadCrumb("[EvaluationAtTimeTask] task setting");

                    Integer timerTaskId = (Integer) taskData.get("timerTaskId");
                    Integer userId = (Integer) taskData.get("userId");
                    Integer relatedUserId = (Integer) taskData.get("relatedUserId");

                    logger.info("DEBUG : TIMER TASK ID : " + timerTaskId);
                    logger.info("DEBUG : USER ID : " + userId);
                    logger.info("DEBUG : relatedUserId ID : " + relatedUserId);

                    sentry.addExtra("timerTaskId", timerTaskId);
                    sentry.addExtra("userId", userId);
                    sentry.addExtra("relatedUserId", relatedUserId);

                    sentry.addBreadCrumb("[EvaluationAtTimeTask] selectTimerTaskById");
                    logger.info("[EvaluationAtTimeTask] selectTimerTaskById");
                    TimerTaskVO timerTaskVO = new TimerTaskDAO().selectTimerTaskById(timerTaskId);
                    if (timerTaskVO == null) {
                        throw new Exception("timerTask is not found");
                    }

                    if (timerTaskVO.getStatus().equals(STATUS_CANCEL)) {
                        throw new Exception("timer task is already canceled");
                    }
                    setTimerTask(timerTaskVO);

                    sentry.addBreadCrumb("[EvaluationAtTimeTask] selectTimerRuleById");
                    logger.info("[EvaluationAtTimeTask] selectTimerRuleById");
                    TimerRuleVO timerRuleVO = new TimerRuleDAO().selectTimerRuleById(timerTaskVO.getTimerRuleId());
                    if (timerRuleVO == null) {
                        throw new Exception("timerRule is not found");
                    }
                    setTimerRule(timerRuleVO);

                    sentry.addBreadCrumb("[EvaluationAtTimeTask] selectUserByUserId");
                    logger.info("[EvaluationAtTimeTask] selectUserByUserId");
                    UserVO userVO = new UserDAO().selectUserByUserId(userId);
                    if (userVO == null) {
                        throw new Exception("user is not found");
                    }

                    if (!userVO.getUserStatus().equals(USER_STATUS_ACTIVE)) {
                        throw new Exception("user status is not active");
                    }
                    setUser(userVO);

                    sentry.addBreadCrumb("[EvaluationAtTimeTask] selectUserRole");
                    logger.info("[EvaluationAtTimeTask] selectUserRole");
                    UserRoleVO userRoleVO = new UserDAO().selectUserRole(userId);
                    if (userRoleVO == null) {
                        throw new Exception("user role is empty");
                    }
                    setUserRole(userRoleVO);

                    UserVO relatedUserVO = new UserDAO().selectUserByUserId(relatedUserId);
                    if (relatedUserVO == null) {
                        throw new Exception("related user is not found");
                    }
                    setRelatedUser(relatedUserVO);

                    sentry.addBreadCrumb("[EvaluationAtTimeTask] selectUserWriteReviewCount");
                    logger.info("[EvaluationAtTimeTask] selectUserWriteReviewCount");
                    Integer reviewCount = new UserDAO().selectUserWriteReviewCount(userId);
                    boolean isFirstReview = isFirstReview(reviewCount);

                    sentry.addExtra("isFirstReview", isFirstReview);

                    sentry.addBreadCrumb("[EvaluationAtTimeTask] updateTimerTaskStatusById AS update");
                    new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, STATUS_PROGRESS);

                    logger.info("DEBUG : updateTimerTaskStatusById in progress" + timerTaskId + " // " + timerRuleVO.getTimerRuleId());

                    logger.info("DEBUG : APPLY ID : " + taskData.get("applyId"));

                    if (taskData.get("applyId") != null) {
                        setApplyId((int) taskData.get("applyId"));
                    } else {
                        // TODO: find applyId
                        Map param = new HashMap();
                        param.put("userId", userId);
                        param.put("relatedUserId", relatedUserId);
                        param.put("regDate", getTimerTask().getRegDate());
                        int applyIdResult = new UserDAO().findApplyIdManually(param);

                        if (applyIdResult == 0) {
                            throw new Exception("invalid applyId");
                        }
                        logger.info("DEBUG : SET APPLY ID AS : " + applyIdResult);
                        setApplyId(applyIdResult);
                    }

                    logger.info("DEBUG : GET APPLY ID  : " + getApplyId());

                    boolean result = false;
                    if (timerRuleVO.getTimerRuleId().equals(52)) {
                        logger.info("DEBUG : RULE ID = 52");
                        sentry.addBreadCrumb("[EvaluationAtTimeTask] : 52");
                        if (isFirstReview) {
                            result = parentFirstReviewEncourage();
                        } else {
                            result = parentReviewEncourage();
                        }
                    }

                    if (timerRuleVO.getTimerRuleId().equals(53)) {
                        sentry.addBreadCrumb("[EvaluationAtTimeTask] : 53");
                        if (isFirstReview) {
                            result = sitterFirstReviewEncourage();
                        } else {
                            result = sitterReviewEncourage();
                        }
                    }

                    if (timerRuleVO.getTimerRuleId().equals(58)) {
                        sentry.addBreadCrumb("[EvaluationAtTimeTask] : 58");
                        if (isFirstReview) {
                            result = parentFirstReviewRemind();
                        } else {
                            result = parentReviewRemind();
                        }
                    }

                    if (timerRuleVO.getTimerRuleId().equals(59)) {
                        sentry.addBreadCrumb("[EvaluationAtTimeTask] : 59");
                        if (isFirstReview) {
                            result = sitterFirstReviewRemind();
                        } else {
                            result = sitterReviewRemind();
                        }
                    }

                    if (!result) {
                        throw new Exception("unknown error");
                    }

                    history.setTo(getUser().getUserPhone());
                    history.setMsgType("TIMER");
                    history.setReceiver(getUser().getUserId());
                    history.setSenderInfo(String.valueOf(getTimerTask().getTimerRuleId()));

                    new MessageHistoryDAO().insertMessageHistory(history);
                    sentry.addBreadCrumb("[EvaluationAtTimeTask] updateTimerTaskStatusById AS done");
                    new TimerTaskDAO().updateTimerTaskStatusById(getTimerTask().getTimerTaskId(), STATUS_DONE);
                } catch (Exception e) {
                    e.printStackTrace();
                    sentry.capture(e);
                    // TODO: status as fail
                    new TimerTaskDAO().updateTimerTaskStatusById(getTimerTask().getTimerTaskId(), STATUS_FAIL);
                }
            }
        };
    }

    public boolean isFirstReview(Integer count) {
        if (count > 0) {
            return false;
        }
        return true;
    }


    public String generateLinkToken() {
        Map<String, Object> param = new HashMap<>();
        param.put("applyId", getApplyId());
        param.put("userId", getRelatedUser().getUserId());
        ReviewVO reviewVO = new UserDAO().selectTargetReview(param);
        if (reviewVO == null) {
            return new LinkToken().getLink(REVIEW_WRITE_PAGE_URL + "?targetId=" + getRelatedUser().getUserId() + "&applyId=" + getApplyId(), getUser().getUserId());
        } else {
            return new LinkToken().getLink(REVIEW_WRITE_PAGE_URL + "?targetId=" + getRelatedUser().getUserId() + "&applyId=" + getApplyId() + "&reviewType=" + reviewVO.getReviewType(), getUser().getUserId());
        }
    }


    // TODO: define message
    // TODO: sending message
    public boolean parentReviewEncourage() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[맘시터 후기쓰기]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + " 맘시터는 어떠셨나요?\n" +
                    "\n" +
                    "- 채용하셨다면 맘시터가 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "후기를 작성 할 때마다, 적립금 1,000원씩 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 부모님들과 맘시터에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean parentFirstReviewEncourage() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[맘시터 후기쓰기]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + " 맘시터는 어떠셨나요?\n" +
                    "\n" +
                    "- 채용하셨다면 맘시터가 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "첫 후기 작성하면, 적립금 2,000원 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 부모님들과 맘시터에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean sitterReviewEncourage() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[부모님 후기쓰기]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + " 부모님은 어떠셨나요?\n" +
                    "\n" +
                    "- 채용되셨다면 부모님이 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "후기를 작성 할 때마다, 적립금 1,000원씩 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 맘시터들과 부모님에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "일하기 좋은 세상 만들어 주셔서 감사합니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean sitterFirstReviewEncourage() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[부모님 후기쓰기]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + " 부모님은 어떠셨나요?\n" +
                    "\n" +
                    "- 채용되셨다면 부모님이 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "첫 후기 작성하면, 적립금 2,000원 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 맘시터들과 부모님에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "일하기 좋은 세상 만들어 주셔서 감사합니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean parentReviewRemind() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[맘시터 후기 마감 1일 전]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + "맘시터의 후기 작성기간이 내일 종료됩니다.\n" +
                    "\n" +
                    "- 채용하셨다면 맘시터가 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "후기를 작성 할 때마다, 적립금 1,000원씩 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 부모님들과 맘시터에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean parentFirstReviewRemind() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[맘시터 후기 마감 1일 전]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + "맘시터의 후기 작성기간이 내일 종료됩니다.\n" +
                    "\n" +
                    "- 채용하셨다면 맘시터가 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "첫 후기 작성하면, 적립금 2,000원 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 부모님들과 맘시터에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean sitterReviewRemind() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[부모님 후기 마감 1일 전]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + " 부모님의 후기 작성기간이 내일 종료됩니다.\n" +
                    "\n" +
                    "- 채용되셨다면 부모님이 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "후기를 작성 할 때마다, 적립금 1,000원씩 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "부모님에 대해 후기를 작성해주세요.\n" +
                    "다른 맘시터들과 부모님에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "일하기 좋은 세상 만들어 주셔서 감사합니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean sitterFirstReviewRemind() {
        try {
            String tokenUrl = generateLinkToken();
            String message = "[부모님 후기 마감 1일 전]\n" +
                    getUser().getUserName() + "님, " + getRelatedUser().getUserName() + "부모님의 후기 작성기간이 내일 종료됩니다.\n" +
                    "\n" +
                    "- 채용되셨다면 부모님이 어떤지 평가해주세요.\n" +
                    "- 인터뷰만 하셨어도, 느낀점을 간단하게 작성해주세요.\n" +
                    "- 연락이 되지 않거나, 잘못된 상황에 대해서도 꼭! 후기를 남겨주세요.\n" +
                    "\n" +
                    "첫 후기 작성하면, 적립금 2,000원 즉시 지급!\n" +
                    "\n" +
                    tokenUrl + "\n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "부모님에 대해 후기를 작성해주세요.\n" +
                    "다른 맘시터들과 부모님에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "일하기 좋은 세상 만들어 주셔서 감사합니다.\n" +
                    "\n" +
                    "대한민국 No.1 아이돌봄, 맘시터";

            history.setMsg(message);
            boolean sendMessageResult = SendLMSMessage.getInstance().sendMessage(getUser().getUserPhone(), URLEncoder.encode(message, "UTF-8"));

            if (!sendMessageResult) {
                throw new Error("cannot send message");
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }

    public void setUserRole(UserRoleVO role) {
        this.userRole = role;
    }

    public UserRoleVO getUserRole() {
        return userRole;
    }

    public UserVO getRelatedUser() {
        return relatedUser;
    }

    public void setRelatedUser(UserVO relatedUser) {
        this.relatedUser = relatedUser;
    }

    public TimerTaskVO getTimerTask() {
        return timerTask;
    }

    public void setTimerTask(TimerTaskVO timerTask) {
        this.timerTask = timerTask;
    }

    public TimerRuleVO getTimerRule() {
        return timerRule;
    }

    public void setTimerRule(TimerRuleVO timerRule) {
        this.timerRule = timerRule;
    }

    public void setApplyId(int applyId) {
        this.applyId = applyId;
    }

    public int getApplyId() {
        return this.applyId;
    }

}
