package com.momsitter.nettyrest.utils.timers;

import com.momsitter.models.dao.*;
import com.momsitter.models.vo.*;
import com.momsitter.nettyrest.utils.AfterRoutine;
import com.momsitter.nettyrest.utils.LMSResponseParser;
import com.momsitter.sentry.SentryLogger;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;
import io.sentry.Sentry;
import net.nurigo.java_sdk.api.Message;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created by seungjaeyuk on 2017. 9. 3..
 */
public class SimpleMessageSender {
    private static final Logger logger = LoggerFactory.getLogger(SimpleMessageSender.class);
    private SentryLogger sentry = new SentryLogger();
    private int timerTaskId;
    private final String API_KEY = "NCS58D23951764C8";
    private final String API_SECRET = "7973AFE810818883CC4F0348358B63EF";
    private final String USER_STATUS_ACTIVE = "active";
    private final String PARENT_PROFILE_ACTIVE = "active";
    private final String SITTER_PROFILE_ACTIVE = "active";
    private final String TIMER_TASK_DONE = "done";
    private final String TIMER_TASK_IN_PROGRESS = "in-progress";
    private final String TIMER_TASK_PENDING = "pending";
    private final String TIMER_TASK_FAIL = "fail";
    private final String TIMER_TASK_CANCEL_POLICY = "cancel-policy";

    public SimpleMessageSender(int timerTaskId) {
        this.timerTaskId = timerTaskId;
    }

    private MessageHistoryVO makeHistoryObject(TimerTaskTargetUserVO targetUser) {
        MessageHistoryVO history = new MessageHistoryVO();
            history.setMsg(targetUser.getMessage());
            history.setMsgType("TIMER");
            history.setReceiver(targetUser.getUserId());
            history.setSender(0);
            history.setSenderInfo(" ");
            history.setTo(targetUser.getUserPhone());
        return history;
    }

    private void runAfterRutine(TimerRuleVO timerRuleVO, TimerTaskTargetUserVO targetUser) {
        try {
            new AfterRoutine().execute(timerRuleVO.getTimerRuleId(), targetUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendATAMessage(TimerRuleVO timerRuleVO, TimerTaskTargetUserVO targetUser) {
        boolean sendingResult = false;
        try {
            // TODO: 메세지를 하나씩 보내지 않고 한번에 보낼 수 있는 방법이 있을까?
            // REF: https://www.coolsms.co.kr/index.php?mid=JAVA_SDK_EXAMPLE_GroupMessage
            Message coolsms = new Message(API_KEY, API_SECRET);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("from", "18336331");
            params.put("type", "ATA");
            params.put("sender_key", "01bff383c2e18a9f171e8aa11cb6ac5f6f1f4012");
            params.put("app_version", "JAVA SDK v2.2");
            params.put("to", targetUser.getUserPhone());
            params.put("text", targetUser.getMessage());
            params.put("template_code", timerRuleVO.getMessageTemplateCode());

            if (targetUser.getButtonName() != null && !targetUser.getButtonName().equals("")) {
                params.put("button_name", targetUser.getButtonName()); // 알림톡 버튼이름 입니다.
            }
            if (targetUser.getButtonLink() != null && !targetUser.getButtonLink().equals("")) {
                params.put("button_url", targetUser.getButtonLink()); // 알림톡 버튼URL 입니다.
            }

            coolsms.send(params);
            sendingResult = true;
        } catch (Exception e) {
            e.printStackTrace();
            sentry.capture(e);
            sendingResult = false;
        }

        try {
            new TimerTaskTargetDAO().updateTaskTargetResult(targetUser.getTaskTargetId(), sendingResult);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            runAfterRutine(timerRuleVO, targetUser);
        } catch(Exception e) {
            e.printStackTrace();
        }
        loggingHistory(targetUser, sendingResult);
    }

    private void sendLMSMessage(TimerRuleVO timerRuleVO, TimerTaskTargetUserVO targetUser) {
        boolean sendingResult = false;
        try {
            String url = "http://rest.supersms.co:6200/sms/xml?" +
                    "id=momsitter" +
                    "&pwd=QKOUK67786LBO78MIXFH" +
                    "&from=18336331" +
                    "&to_country=82" +
                    "&to=" + targetUser.getUserPhone() +
                    "&report_req=0" +
                    "&message=" + targetUser.getMessage();
            HttpClient httpClient = new DefaultHttpClient();
            HttpRequestBase httpGet = new HttpGet(url);
            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                sendingResult = LMSResponseParser.getInstance().isSuccess(EntityUtils.toString(entity));
            } else {
                sendingResult = false;
            }
        } catch (Exception e) {
            sentry.capture(e);
            e.printStackTrace();
            sendingResult = false;
        }

        try {
            new TimerTaskTargetDAO().updateTaskTargetResult(targetUser.getTaskTargetId(), sendingResult);
        } catch (Exception e) {
            sentry.capture(e);
            e.printStackTrace();
        }
        try {
            runAfterRutine(timerRuleVO, targetUser);
        } catch(Exception e) {
            sentry.capture(e);
            e.printStackTrace();
        }

        try {
            targetUser.setMessage(URLDecoder.decode(targetUser.getMessage(), "UTF-8"));
        } catch (Exception e) {
            sentry.capture(e);
            e.printStackTrace();
        }

        loggingHistory(targetUser, sendingResult);
    }

    private void loggingHistory(TimerTaskTargetUserVO targetUser, boolean sendingResult) {
        if (sendingResult) {
            try {
                new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_DONE);
            } catch (Exception e) {
                sentry.capture(e);
                e.printStackTrace();
            }

            try {
                new MessageHistoryDAO().insertMessageHistory(makeHistoryObject(targetUser));
            } catch (Exception e) {
                sentry.capture(e);
                e.printStackTrace();
            }
        } else {
            try {
                new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_FAIL);
            } catch(Exception e) {
                sentry.capture(e);
                e.printStackTrace();
            }
        }
    }

    public boolean isParent(Integer userTypeId) {
        boolean result = false;
        if (userTypeId == 5) {
            result = true;
        }
        return result;
    }

    public TimerTask init() {
        return new TimerTask() {
            @Override
            public void run(Timeout timeout) throws Exception {
                sentry.addBreadCrumb("[SimpleMessageSender] start");

                sentry.addExtra("timerTaskId", timerTaskId);

                sentry.addBreadCrumb("[SimpleMessageSender] selectTimerTaskById");
                try {
                    TimerTaskVO timerTaskVO = new TimerTaskDAO().selectTimerTaskById(timerTaskId);

                    sentry.addBreadCrumb("[SimpleMessageSender] selectTimerRuleById");
                    TimerRuleVO timerRuleVO = new TimerRuleDAO().selectTimerRuleById(timerTaskVO.getTimerRuleId());

                    if (timerTaskVO != null && timerRuleVO != null) {
                        sentry.addBreadCrumb("[SimpleMessageSender] selectTimerTaskTargetUserByTimerTaskId");
                        List<TimerTaskTargetUserVO> timerTaskTargets = new TimerTaskTargetDAO().selectTimerTaskTargetUserByTimerTaskId(timerTaskId);

                        if (timerTaskVO.getStatus().equals(TIMER_TASK_PENDING)) {

                            if (!timerTaskTargets.isEmpty()) {
                                TimerTaskTargetUserVO targetUser = null;
                                sentry.addBreadCrumb("[SimpleMessageSender] updateTimerTaskStatusById as update");
                                new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_IN_PROGRESS);

                                for (int i = 0; i < timerTaskTargets.size(); i += 1) {
                                    targetUser = timerTaskTargets.get(i);

                                    sentry.addBreadCrumb("[SimpleMessageSender] selectUserByUserId");
                                    UserVO user = new UserDAO().selectUserByUserId(targetUser.getUserId());

                                    // NOTE : selectUserRole 함수는 사용자 별로 1개의 role 을 가지고 있다고 가정하고 있음.
                                    sentry.addBreadCrumb("[SimpleMessageSender] selectUserRole");
                                    UserRoleVO userRole = new UserDAO().selectUserRole(targetUser.getUserId());

                                    if (user == null) {
                                        throw new Error("cannot find user");
                                    }

                                    if (userRole == null) {
                                        throw new Error("cannot find userRole");
                                    }

                                    if (!user.getUserStatus().equals(USER_STATUS_ACTIVE)) {
                                        throw new Error("user status is not active");
                                    }

                                    if (targetUser.getMessage() == null) {
                                        throw new Error("empty message");
                                    }

                                    if (targetUser.getMessage().equals("")) {
                                        throw new Error("empty message");
                                    }

                                    if (timerRuleVO.getMessageType() == null || timerRuleVO.getMessageType().equals("ATA") || timerRuleVO.getMessageType().equals(null)) {
                                        // NOTE : ATA 방식의 예약 메세지
                                        // NOTE 부모 구인 마감인 경우 profileStatus 가 active 일때만 발송
                                        if (timerRuleVO.getTimerRuleId().equals(3)) {
                                            if (isParent(userRole.getUserTypeId())) {
                                                ParentProfile parentProfile = new UserDAO().selectParentProfileByUserId(targetUser.getUserId());
                                                if (parentProfile != null) {
                                                    if (parentProfile.getProfileStatus().equals(PARENT_PROFILE_ACTIVE)) {
                                                        sendATAMessage(timerRuleVO, targetUser);
                                                    } else {
                                                        // ERROR : 프로필이 ACTIVE가 아님
                                                        sentry.addBreadCrumb("[SimpleMessageSender] updateTimerTaskStatusById as cancel policy");
                                                        new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_CANCEL_POLICY);
                                                    }
                                                } else {
                                                    // ERROR : 부모회원의 프로필이 존재하지 않음
                                                    sentry.addBreadCrumb("[SimpleMessageSender] updateTimerTaskStatusById as cancel policy");
                                                    new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_CANCEL_POLICY);
                                                }
                                            } else {
                                                // ERROR : 부모권한일때만 발송되야하는데 시터회원임
                                                sentry.addBreadCrumb("[SimpleMessageSender] updateTimerTaskStatusById as cancel policy");
                                                new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_CANCEL_POLICY);
                                            }
                                        } else {
                                            sentry.addBreadCrumb("[SimpleMessageSender] sendATAMessage");
                                            sendATAMessage(timerRuleVO, targetUser);
                                        }
                                    }
                                    if (timerRuleVO.getMessageType() != null) {
                                        if (timerRuleVO.getMessageType().equals("LMS")) {
                                            // NOTE : LMS 방식의 예약 메세지
                                            if (timerRuleVO.equals(15) || timerRuleVO.equals(16) || timerRuleVO.equals(17)) {
                                                SitterProfile sitterProfile = new UserDAO().selectSitterProfileByUserId(targetUser.getUserId());
                                                if (sitterProfile != null) {
                                                    if (sitterProfile.getProfileStatus().equals(SITTER_PROFILE_ACTIVE)) {
                                                        sendLMSMessage(timerRuleVO, targetUser);
                                                    } else {
                                                        sentry.addBreadCrumb("[SimpleMessageSender] updateTimerTaskStatusById as cancel policy");
                                                        new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_CANCEL_POLICY);
                                                    }
                                                } else {
                                                    sentry.addBreadCrumb("[SimpleMessageSender] updateTimerTaskStatusById as cancel policy");
                                                    new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, TIMER_TASK_CANCEL_POLICY);
                                                }
                                            } else {
                                                sentry.addBreadCrumb("[SimpleMessageSender] sendLMSMessage");
                                                sendLMSMessage(timerRuleVO, targetUser);
                                            }
                                        }
                                    }
                                }
                            } else {
                                throw new Exception("timer task target is empty");
                            }
                        } else {
                            throw new Exception("timer task status is not pending");
                        }
                    }

                } catch (Exception e) {
                    sentry.capture(e);
                    new TimerTaskDAO().updateTimerTaskStatusById(timerTaskId, "fail");
                }
            }
        };
    }

    public int getTimerTaskId() {
        return timerTaskId;
    }

    public void setTimerTaskId(int timerTaskId) {
        this.timerTaskId = timerTaskId;
    }
}
