package com.momsitter.nettyrest.utils;

import com.momsitter.models.vo.TimerTaskTargetUserVO;

public class TimerMessageUtil {
    private static TimerMessageUtil instance = null;

    private TimerMessageUtil (){}

    public static TimerMessageUtil getInstance() {
        if (instance == null) {
            instance = new TimerMessageUtil();
        }
        return instance;
    }

    public String getMessageByTimerRuleId (Integer timerRuleId, TimerTaskTargetUserVO user) {
        String message = "";

        if (timerRuleId.equals(1)) {
            message = "???";
        }

        // 시터 인증 장려
        if (timerRuleId.equals(2)) {
            message = "시터 인증 장려";
        }

        // 부모 구인 마감 PAplyEnd02
        if (timerRuleId.equals(3)) {
            message = "[신청서 구인종료]\n" +
                    //"#{부모이름}님, 등록하셨던 신청서가 아래 이유로 '구인종료' 되었습니다.\n" +
                    user.getUserName() + "님, 등록하셨던 신청서가 아래 이유로 '구인종료' 되었습니다.\n" +
                    "\n" +
                    "- 희망 돌봄 기간 종료\n" +
                    "- 신청서 작성후 1주일 경과\n" +
                    "\n" +
                    "‘구인중’ 상태로 변경 원하시면, 신청서의 ‘돌봄기간’을 수정해주시거나 \n" +
                    "마이페이지> 신청서 상태> 구인중으로 변경해주세요.\n" +
                    "\n" +
                    "아이 키우기 힘든 세상, \n" +
                    "맘시터가 해결하겠습니다. \n";
        }

        // 시터 지원 응답 장려 PSAplyRE1
        if (timerRuleId.equals(4)) {
            message =
                    //"[주의!] #{시터이름}님, 2시간전 받으신 맘시터 신청 응답시간이 얼마 남지 않았습니다!\n" +
                    "[주의!] " + user.getUserName() + "님, 2시간전 받으신 맘시터 신청 응답시간이 얼마 남지 않았습니다!\n" +
                    "\n" +
                    "부모님께서 애타게 기다리고 있답니다ㅠㅠ 지금 바로 아래 버튼을 통해 신청서에 응답해주세요!\n" +
                    "\n" +
                    "*맘시터 로그인 > 마이페이지 > 내가 받은 신청서에서도 확인 가능합니다.\n" +
                    "\n" +
                    "무응답이 계속되면 활동에 제한을 받을 수 있습니다.";
        }

        // 부모 지원 응답 장려 StrApyRd1
        if (timerRuleId.equals(5)) {
            message = "[주의!] 맘시터가 보낸 지원에 응답해주세요! 응답시간이 얼마남지 않았습니다. \n" +
                    "\n" +
                    "맘시터 프로필을 보고 마음에 든다면, 연락처를 열람하여 자유롭게 연락해보세요^^\n" +
                    "\n" +
                    "만약 마음에 들지 않는다면, 거절 사유를 시터에게 알려주세요ㅠㅠ\n" +
                    "\n" +
                    "*맘시터 로그인 > 마이페이지 > 내가 받은 지원에서 확인 가능합니다.\n" +
                    "www.mom-sitter.com";
        }

        // 부모 후기 작성 요청 PRvwCall
        if (timerRuleId.equals(6)) {
            message = "[맘시터 후기 요청]\n" +
                    //"#{부모이름}님, 연락처를 받으셨던 #{시터이름} 선생님의 후기 요청이 도착하였습니다. \n" +
                    "#{부모이름}님, 연락처를 받으셨던 " + user.getUserName() + " 선생님의 후기 요청이 도착하였습니다. \n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "맘시터에 대해 후기를 작성해주세요.\n" +
                    "다른 부모님들과 맘시터에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "- 인터뷰만 하셨어도, 느낀 점들을 간단하게 작성해주세요.\n" +
                    "- 채용이 되셨다면, 좀 더 상세하게 알려주세요^^\n" +
                    "\n" +
                    "아이 키우기 좋은 세상 만들어주셔서 감사합니다.";
        }

        // 시터 후기 작성 요청 SReview01
        if (timerRuleId.equals(7)) {
            message = "[부모 후기 요청]\n" +
                    //"매칭되셨던 #{부모이름} 부모님의 후기 요청이 도착하였습니다. \n" +
                    "매칭되셨던 " + user.getUserName() + " 부모님의 후기 요청이 도착하였습니다. \n" +
                    "\n" +
                    "1분만 투자하여,\n" +
                    "부모님에 대해 알려주세요.\n" +
                    "다른 맘시터들과 부모님에게 큰 힘이 됩니다.\n" +
                    "\n" +
                    "- 인터뷰만 하셨어도, 느낀 점들을 간단하게 작성해주세요.\n" +
                    "- 채용이 되셨다면, 좀 더 상세하게 알려주세요^^\n" +
                    "\n" +
                    "일하기 좋은 세상 만들어 주셔서 감사합니다.";
        }

        // 시터 프로필 업데이트 권고
        if (timerRuleId.equals(8)) {
            message = "시터 프로필 업데이트 권고";
        }

        return message;
    }

}
