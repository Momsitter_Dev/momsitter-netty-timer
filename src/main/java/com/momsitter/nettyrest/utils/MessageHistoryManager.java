package com.momsitter.nettyrest.utils;

import com.momsitter.models.dao.MessageHistoryDAO;
import com.momsitter.models.vo.MessageHistoryVO;

public class MessageHistoryManager {
    private MessageHistoryVO messageHistoryVO;
    public MessageHistoryManager() {
        messageHistoryVO = new MessageHistoryVO();
    }
    public void setSender(Integer sender) {
        messageHistoryVO.setSender(sender);
    }
    public void setReceiver(Integer receiver) {
        messageHistoryVO.setReceiver(receiver);
    }
    public void setMsgType(String msgType) {
        messageHistoryVO.setMsgType(msgType);
    }
    public void setTo(String to) {
        messageHistoryVO.setTo(to);
    }
    public void setMsg(String msg) {
        messageHistoryVO.setMsg(msg);
    }
    public void setSendDate(String sendDate) {
        messageHistoryVO.setSendDate(sendDate);
    }
    public void setSenderInfo(String senderInfo) {
        messageHistoryVO.setSenderInfo(senderInfo);
    }

    public void save() {
        try {
            new MessageHistoryDAO().insertMessageHistory(messageHistoryVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

