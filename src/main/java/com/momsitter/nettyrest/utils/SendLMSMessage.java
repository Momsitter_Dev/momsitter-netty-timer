package com.momsitter.nettyrest.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendLMSMessage {
    private static final Logger logger = LoggerFactory.getLogger(SendLMSMessage.class);
    private static SendLMSMessage instance;
    private SendLMSMessage () { }

    public static SendLMSMessage getInstance () {
        if (instance == null) {
            instance = new SendLMSMessage();
        }
        return instance;
    }

    public boolean sendMessage (String userPhone, String message) {
        boolean result = false;
        try {
            String url = "http://rest.supersms.co:6200/sms/xml?" +
                    "id=momsitter" +
                    "&pwd=QKOUK67786LBO78MIXFH" +
                    "&from=18336331" +
                    "&to_country=82" +
                    "&to=" + userPhone +
                    "&report_req=0" +
                    "&title=title" +
                    "&message=" + message;

            HttpClient httpClient = new DefaultHttpClient();
            HttpRequestBase httpGet = new HttpGet(url);
            HttpResponse response = httpClient.execute(httpGet);

            // TODO RESPONSE PARSING
            logger.info(response.toString());
            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                // TODO: Parsing response document as XML
                int size = 0;
                byte [] buffer = new byte[1024];
                while((size = entity.getContent().read(buffer)) != -1) {
                    System.out.write(buffer, 0, size);
                }
                /*
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    Document document = builder.parse(entity.getContent());
                    document.get
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.toString());
                }
                */
                result = true;
            } else {
                logger.info("MESSAGE SEND FAIL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("SEND LMS MESSAGE : END : " + result);
        return result;
    }
}
