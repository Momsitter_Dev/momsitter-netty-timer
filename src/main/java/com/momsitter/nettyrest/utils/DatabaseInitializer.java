package com.momsitter.nettyrest.utils;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by seungjaeyuk on 2017. 9. 3..
 */
public class DatabaseInitializer {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);
    private static SqlSessionFactory sqlSessionFactory;
    private static DatabaseInitializer instance;

    private DatabaseInitializer () {
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml"));
    }

    public static DatabaseInitializer getInstance() {
        if (instance == null) {
            instance = new DatabaseInitializer();
        }
        return instance;
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

    public boolean init() {
        boolean result = false;
        try {
            logger.info("init DB");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(this.getClass().getClassLoader().getResourceAsStream("mybatis-config.xml"));
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("init DB FAIL");
        }
        return result;
    }
}
