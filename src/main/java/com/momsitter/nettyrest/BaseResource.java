package com.momsitter.nettyrest;

import com.momsitter.nettyrest.response.Info;
import com.momsitter.nettyrest.response.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * api resource base method
 */
public class BaseResource {

    protected Logger logger;

    protected ApiProtocol apiProtocol;

    public BaseResource(ApiProtocol apiProtocol) {
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.apiProtocol = apiProtocol;
    }

    public Object parameterIntCheck(ApiProtocol apiProtocol, String parameter) {
        if (apiProtocol.getParameters().containsKey(parameter)) {
            try {
                return Integer.parseInt(apiProtocol.getParameters().get(parameter).get(0));
            } catch (NumberFormatException e) {
                logger.error(e.getMessage());
                return error(StatusCode.PARAM_FORMAT_ERROR, parameter);
            }
        } else {
            return error(StatusCode.PARAM_CAN_NOT_BE_NULL, parameter);
        }
    }

    public Object getParam(ApiProtocol apiProtocol, String param) {

        if (apiProtocol.getParameters().containsKey(param)) {
            logger.info("DEBUG : PARAM : " + apiProtocol.getParameters().get(param));
            logger.info("DEBUG : PARAM : " + apiProtocol.getParameters().get(param).get(0));
            return apiProtocol.getParameters().get(param).get(0);
        } else {
            return error(StatusCode.PARAM_CAN_NOT_BE_NULL);
        }
    }

    public String getStringParam(ApiProtocol apiProtocol, String param) {
        String result = "";
        if (apiProtocol.getParameters().containsKey(param)) {
            try {
                result = apiProtocol.getParameters().get(param).get(0);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("DEBUG : DEBUG PARAMETER STRING : " + result);
        return result;
    }


    protected Result error(int code) {
        return ErrorHandler.error(code);
    }

    protected Result error(int code, String parameter) {
        return ErrorHandler.error(code, parameter);
    }

    protected Result success() {
        return new Result<>(new Info());
    }

    protected Result success(int code) {
        Result result = new Result<>(new Info());
        result.getInfo().setCode(code).setCodeMessage(StatusCode.codeMap.get(code));
        return result;
    }

}
