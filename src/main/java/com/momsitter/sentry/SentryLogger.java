package com.momsitter.sentry;

/*
import io.sentry.Sentry;
import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SentryLogger {
    private static final Logger logger = LoggerFactory.getLogger(SentryLogger.class);
    //private SentryClient sentry;
    public SentryLogger() {
        /*
        try {
            Sentry.init("https://3c9226019f33410badff60a41567f2cc@sentry.io/1217449");
            sentry = SentryClientFactory.sentryClient();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    public void addBreadCrumb(String message) {
        try {
            /*
            Sentry.getContext().recordBreadcrumb(
                    new BreadcrumbBuilder().setMessage(message).build()
            );
            */
        } catch (Exception e) {
            logger.info(message);
            e.printStackTrace();
        }
    }

    public void setUserId(Integer userId) {
        try {
            /*
            Sentry.getContext().setUser(
                    new UserBuilder().setId(String.valueOf(userId)).build()
            );
            */
        } catch (Exception e) {
            logger.info(String.valueOf(userId));
            e.printStackTrace();
        }
    }

    public void setUserId(String userId) {
        try {
            /*
            Sentry.getContext().setUser(
                    new UserBuilder().setId(userId).build()
            );
            */
        } catch (Exception e) {
            logger.info(userId);
            e.printStackTrace();
        }
    }

    public void setUserName(String userName) {
        try {
            /*
            Sentry.getContext().setUser(
                    new UserBuilder().setUsername(userName).build()
            );
            */
        } catch (Exception e) {
            logger.info(userName);
            e.printStackTrace();
        }
    }

    public void addExtra(String name, String value) {
        try {
            //Sentry.getContext().addExtra(name, value);
        } catch (Exception e) {
            logger.info(name + " // " + value);
            e.printStackTrace();
        }
    }

    public void addExtra(String name, boolean value) {
        try {
            //Sentry.getContext().addExtra(name, value);
        } catch (Exception e) {
            logger.info(name + " // " + value);
            e.printStackTrace();
        }
    }

    public void addExtra(String name, long value) {
        try {
            //Sentry.getContext().addExtra(name, String.valueOf(value));
        } catch (Exception e) {
            logger.info(name + " // " + value);
            e.printStackTrace();
        }
    }

    public void capture(Exception e) {
        try {
            logger.error(e.getMessage());
            //Sentry.capture(e);
        } catch (Exception err) {
            e.printStackTrace();
        }
    }

}
