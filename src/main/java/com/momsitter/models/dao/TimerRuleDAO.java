package com.momsitter.models.dao;

import com.momsitter.models.vo.TimerRuleVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import com.momsitter.models.mapper.TimerRuleMapper;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by seungjaeyuk on 2017. 9. 3..
 */
public class TimerRuleDAO implements TimerRuleMapper {
    private static final Logger logger = LoggerFactory.getLogger(TimerRuleDAO.class);
    private SqlSession sqlSession;

    @Override
    public List<TimerRuleVO> getAll() {
        List<TimerRuleVO> result = new ArrayList<TimerRuleVO>();
        logger.info("momsitter:TimerRuleDAO => getAll : START");
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("timerRule.getAll");
        } catch (Exception e) {
            logger.info("momsitter:TimerRuleDAO => getAll : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        logger.info("momsitter:TimerRuleDAO => getAll : END ResultLength(" + result.size() + ")");
        return result;
    }

    @Override
    public TimerRuleVO selectTimerRuleById(Integer timerRuleId) {
        TimerRuleVO result = null;
        logger.info("momsitter:TimerRuleDAO => selectTimerRuleById : START");
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("timerRule.selectTimerRuleById", timerRuleId);
        } catch (Exception e) {
            logger.info("momsitter:TimerRuleDAO => selectTimerRuleById : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        logger.info("momsitter:TimerRuleDAO => selectTimerRuleById : END " + result.toString() + ")");
        return result;
    }
}
