package com.momsitter.models.dao;

import com.momsitter.models.mapper.CronMapper;
import com.momsitter.models.vo.CouponVO;
import com.momsitter.models.vo.ExpireCouponVO;
import com.momsitter.models.vo.FirstPaymentEncourageVO;
import com.momsitter.models.vo.UserProductRemindVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CronDAO implements CronMapper {
    private static final Logger logger = LoggerFactory.getLogger(CronDAO.class);
    private SqlSession sqlSession;

    @Override
    public List<ExpireCouponVO> selectExpireCoupon() {
        List<ExpireCouponVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("cron.selectExpireCoupon", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<UserProductRemindVO> selectUserProductRemind() {
        List<UserProductRemindVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("cron.selectUserProductRemind", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Integer insertNewCoupon(CouponVO coupon) {
        boolean result = false;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("cron.insertNewCoupon", coupon);
            sqlSession.commit();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return coupon.getCouponId();
    }

    @Override
    public List<FirstPaymentEncourageVO> selectFirstPaymentEncourageTarget() {
        List<FirstPaymentEncourageVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("cron.selectFirstPaymentEncourageTarget", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
