package com.momsitter.models.dao;

import com.momsitter.models.mapper.UserWeekdayTimeIndexMapper;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class UserWeekdayTimeIndexDAO implements UserWeekdayTimeIndexMapper {
    private static final Logger logger = LoggerFactory.getLogger(UserWeekdayTimeIndexDAO.class);
    private SqlSession sqlSession;


    public void deleteAndInsertIndex(Integer userId, Map<String, Object> param) {
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.delete("userWeekdayTimeIndex.deleteIndexByUserId", userId);
            sqlSession.insert("userWeekdayTimeIndex.insertIndex", param);
            sqlSession.commit();
        } catch (Exception e) {

        } finally {
            sqlSession.close();
        }
    }


    @Override
    public void deleteIndexByUserId(Integer userId) {
        logger.info("momsitter:UserWeekdayTimeIndexDAO => deleteIndexByUserId : START");
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.delete("userWeekdayTimeIndex.deleteIndexByUserId", userId);
            sqlSession.commit();
        } catch (Exception e){
            logger.info("momsitter:UserWeekdayTimeIndexDAO => deleteIndexByUserId : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void insertIndex(Map<String, Object> param) {
        logger.info("momsitter:UserWeekdayTimeIndexDAO => insertIndex : START");
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("userWeekdayTimeIndex.insertIndex", param);
            sqlSession.commit();
        } catch (Exception e){
            logger.info("momsitter:UserWeekdayTimeIndexDAO => insertIndex : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
