package com.momsitter.models.dao;

import com.momsitter.models.mapper.LogBatchJobMapper;
import com.momsitter.models.vo.LogBatchJobVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogBatchJobDAO implements LogBatchJobMapper {
    private static final Logger logger = LoggerFactory.getLogger(LogBatchJobDAO.class);
    private SqlSession sqlSession;

    @Override
    public void insertLogBatchJob(LogBatchJobVO log) {
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("logBatchJob.insertLogBatchJob", log);
            sqlSession.commit();
        } catch (Exception e) {
            logger.info("momsitter:MessageHistoryDAO => insertMessageHistory : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
