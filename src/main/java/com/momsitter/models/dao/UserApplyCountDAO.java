package com.momsitter.models.dao;

import com.momsitter.models.mapper.UserApplyCountMapper;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

public class UserApplyCountDAO implements UserApplyCountMapper {
    private static final Logger logger = LoggerFactory.getLogger(UserApplyCountDAO.class);
    private SqlSession sqlSession;

    @Override
    public void updateCountAsZero() {
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.update("userApplyCount.updateCountAsZero");
            sqlSession.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateCountByUserId(Integer userId, Integer applyToSitterCount, Integer applyToParentCount) {
        Map param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("applyToSitterCount", applyToSitterCount);
            param.put("applyToParentCount", applyToParentCount);
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.update("userApplyCount.updateCountByUserId", param);
            sqlSession.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
