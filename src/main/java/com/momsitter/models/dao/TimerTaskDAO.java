package com.momsitter.models.dao;
import com.momsitter.models.mapper.TimerTaskMapper;
import com.momsitter.models.vo.TimerTaskVO;
import com.momsitter.models.vo.TimerTaskWithTargetVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

/**
 * Created by seungjaeyuk on 2017. 9. 3..
 */
public class TimerTaskDAO implements TimerTaskMapper {
    private static final Logger logger = LoggerFactory.getLogger(TimerTaskDAO.class);
    private SqlSession sqlSession;

    @Override
    public TimerTaskVO selectTimerTaskById(int timerTaskId) {
        logger.info("momsitter:TimerTaskDAO => selectTimerTaskById (" + timerTaskId + ")");
        TimerTaskVO result = new TimerTaskVO();
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("timerTask.selectTimerTaskById", timerTaskId);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void updateTimerTaskStatusById(int timerTaskId, String status) {
        logger.info("momsitter:TimerTaskDAO => updateTimerTaskStatusById (" + timerTaskId + ", " + status + ")");
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("timerTaskId", timerTaskId);
        param.put("status", status);
        if (status.equals("done")) {
            param.put("isDone", true);
        }

        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.update("timerTask.updateTimerTaskStatusById", param);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public TimerTaskVO insertTimerTask(Integer timerRuleId, String reservedDate) {
        logger.info("momsitter:TimerTaskDAO => insertTimerTask (" + timerRuleId + ", " + reservedDate + ")");
        TimerTaskVO timerTaskVO = new TimerTaskVO();
        timerTaskVO.setTimerRuleId(timerRuleId);
        timerTaskVO.setReservedDate(reservedDate);
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("timerTask.insertTimerTask", timerTaskVO);
            sqlSession.commit();
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return timerTaskVO;
    }

    @Override
    public List<TimerTaskWithTargetVO> getPendingTimerTasks() {
        logger.info("momsitter:TimerTaskDAO => getPendingTimerTasks");
        List<TimerTaskWithTargetVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("timerTask.getPendingTimerTasks", null);
            logger.info("momsitter:TimerTaskDAO => getPendingTimerTasks : SIZE = " + result.size());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        logger.info("momsitter:TimerTaskDAO => getPendingTimerTasks");
        return result;
    }
}
