package com.momsitter.models.dao;

import com.momsitter.models.mapper.TimerTaskTargetMapper;
import com.momsitter.models.vo.TimerTaskTargetUserVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import com.momsitter.models.vo.TimerTaskTargetVO;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by seungjaeyuk on 2017. 9. 4..
 */
public class TimerTaskTargetDAO implements TimerTaskTargetMapper {
    private static final Logger logger = LoggerFactory.getLogger(TimerTaskTargetDAO.class);
    private SqlSession sqlSession;
    @Override
    public List<TimerTaskTargetVO> selectTimerTaskTargetByTimerTaskId(Integer timerTaskId) {
        logger.info("momsitter:TimerTaskTargetDAO => selectTimerTaskTargetByTimerTaskId( (" + timerTaskId + ")");
        List<TimerTaskTargetVO> result = new ArrayList<TimerTaskTargetVO>();
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("timerTaskTarget.selectTimerTaskTargetByTimerTaskId", timerTaskId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public boolean insertTimerTaskTarget(Integer timerTaskId, Integer userId, String message, String buttonName, String buttonLink, Integer relatedUserId) {
        boolean result = false;
        logger.info("momsitter:TimerTaskTargetDAO => insertTimerTaskTarget( (" + timerTaskId + ", " + userId + ")");
        Map<String, Object> param = new HashMap<>();
        param.put("timerTaskId", timerTaskId);
        param.put("userId", userId);
        param.put("message", message);
        param.put("buttonName", buttonName);
        param.put("buttonLink", buttonLink);
        param.put("relatedUserId", relatedUserId);
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("timerTaskTarget.insertTimerTaskTarget", param);
            sqlSession.commit();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void insertTimerTaskTargets(Integer timerTaskId, List<Integer> userId) {
        logger.info("momsitter:TimerTaskTargetDAO => insertTimerTaskTargets (" + timerTaskId + ", " + userId + ")");
        Map<String, Object> param = new HashMap<>();
        param.put("timerTaskId", timerTaskId);
        param.put("userId", userId);
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("timerTaskTarget.insertTimerTaskTargets", param);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<TimerTaskTargetUserVO> selectTimerTaskTargetUserByTimerTaskId(Integer timerTaskId) {
        logger.info("momsitter:TimerTaskTargetDAO => selectTimerTaskTargetUserByTimerTaskId (" + timerTaskId + ")");
        List<TimerTaskTargetUserVO> users = new ArrayList<>();
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            users = sqlSession.selectList("timerTaskTarget.selectTimerTaskTargetUserByTimerTaskId", timerTaskId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return users;
    }

    @Override
    public void updateTaskTargetResult(Integer taskTargetId, boolean result) {
        logger.info("momsitter:TimerTaskTargetDAO => updateTaskTargetResult (" + taskTargetId + "," + result + ")");
        Map<String, Object> param = new HashMap<>();
        param.put("taskTargetId", taskTargetId);
        param.put("result", result);
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.update("timerTaskTarget.updateTaskTargetResult", param);
            sqlSession.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
