package com.momsitter.models.dao;

import com.momsitter.models.mapper.UserMapper;
import com.momsitter.models.vo.*;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class UserDAO implements UserMapper {
    private static final Logger logger = LoggerFactory.getLogger(UserDAO.class);
    private SqlSession sqlSession;

    @Override
    public ParentProfile selectParentProfileByUserId(Integer userId) {
        ParentProfile result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("user.selectParentProfileByUserId", userId);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public SitterProfile selectSitterProfileByUserId(Integer userId) {
        SitterProfile result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("user.selectSitterProfileByUserId", userId);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public UserRoleVO selectUserRole(Integer userId) {
        UserRoleVO result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("user.selectUserRole", userId);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public UserVO selectUserByUserId(Integer userId) {
        UserVO result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("user.selectUserByUserId", userId);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public void updateParentProfileStatus(Map param) {
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.update("user.updateParentProfileStatus", param);
            sqlSession.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateSitterProfileStatus(Map param) {
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.update("user.updateSitterProfileStatus", param);
            sqlSession.commit();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<RemindSitters> selectRemindSitter() {
        List<RemindSitters> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("user.selectRemindSitter");
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public Integer selectUserWriteReviewCount(Integer userId) {
        Integer count = 0;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            count = (int) sqlSession.selectOne("user.selectUserWriteReviewCount", userId);
        } catch (Exception e){
            e.printStackTrace();
            count = 0;
        } finally {
            sqlSession.close();
        }
        return count;
    }

    @Override
    public ReviewVO selectTargetReview(Map param) {
        ReviewVO result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectOne("user.selectTargetReview", param);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public int findApplyIdManually(Map param) {
        int applyId = 0;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            applyId = sqlSession.selectOne("user.findApplyIdManually", param);
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return applyId;
    }
}
