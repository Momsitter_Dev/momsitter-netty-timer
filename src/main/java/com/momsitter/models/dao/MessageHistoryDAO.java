package com.momsitter.models.dao;

import com.momsitter.models.mapper.MessageHistoryMapper;
import com.momsitter.models.vo.MessageHistoryVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageHistoryDAO implements MessageHistoryMapper {
    private static final Logger logger = LoggerFactory.getLogger(MessageHistoryDAO.class);
    private SqlSession sqlSession;

    @Override
    public boolean insertMessageHistory(MessageHistoryVO history) {
        logger.info("momsitter:MessageHistoryDAO => insertMessageHistory : START");
        boolean result = false;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            sqlSession.insert("messageHistory.insertMessageHistory", history);
            sqlSession.commit();
            result = true;
        } catch (Exception e) {
            logger.info("momsitter:MessageHistoryDAO => insertMessageHistory : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        logger.info("momsitter:MessageHistoryDAO => insertMessageHistory : END = (" + result + ")");
        return result;
    }
}
