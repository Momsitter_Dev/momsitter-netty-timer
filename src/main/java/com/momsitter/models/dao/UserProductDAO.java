package com.momsitter.models.dao;

import com.momsitter.models.mapper.UserProductMapper;
import com.momsitter.models.vo.UserProductVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserProductDAO implements UserProductMapper {
    private static final Logger logger = LoggerFactory.getLogger(UserProductDAO.class);
    private SqlSession sqlSession;

    @Override
    public List<UserProductVO> selectNonExpiredUserProducts() {
        logger.info("momsitter:UserProductDAO => selectNonExpiredUserProducts : START");
        List<UserProductVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("userProduct.selectNonExpiredUserProducts");
            sqlSession.commit();
        } catch (Exception e){
            logger.info("momsitter:UserProductDAO => selectNonExpiredUserProducts : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

}
