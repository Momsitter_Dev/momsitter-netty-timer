package com.momsitter.models.dao;

import com.momsitter.models.mapper.MessageHistoryMapper;
import com.momsitter.models.mapper.ProductMapper;
import com.momsitter.models.vo.MessageHistoryVO;
import com.momsitter.models.vo.ProductVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ProductDAO implements ProductMapper {
    private static final Logger logger = LoggerFactory.getLogger(ProductDAO.class);
    private SqlSession sqlSession;

    @Override
    public List<ProductVO> getAllProduct() {
        logger.info("momsitter:ProductDAO => getAllProduct : START");
        List<ProductVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("product.getAllProduct", null);
        } catch (Exception e){
            logger.info("momsitter:ProductDAO => getAllProduct : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }
}
