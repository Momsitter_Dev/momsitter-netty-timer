package com.momsitter.models.dao;

import com.momsitter.models.mapper.UserScheduleMapper;
import com.momsitter.models.vo.UserIdVO;
import com.momsitter.models.vo.UserScheduleVO;
import com.momsitter.nettyrest.utils.DatabaseInitializer;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserScheduleDAO implements UserScheduleMapper {
    private static final Logger logger = LoggerFactory.getLogger(UserScheduleDAO.class);
    private SqlSession sqlSession;

    @Override
    public List<UserIdVO> selectUserIds() {
        logger.info("momsitter:UserScheduleDAO => selectUserIds : START");
        List<UserIdVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("userSchedule.selectUserIds");
        } catch (Exception e){
            logger.info("momsitter:UserScheduleDAO => selectUserIds : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public List<UserScheduleVO> selectUserScheduleByUserId(Integer userId) {
        logger.info("momsitter:UserScheduleDAO => selectUserScheduleByUserId : START");
        List<UserScheduleVO> result = null;
        try {
            sqlSession = DatabaseInitializer.getSqlSessionFactory().openSession();
            result = sqlSession.selectList("userSchedule.selectUserScheduleByUserId", userId);
        } catch (Exception e){
            logger.info("momsitter:UserScheduleDAO => selectUserScheduleByUserId : ERROR");
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }
}
