package com.momsitter.models.mapper;

import java.util.List;

import com.momsitter.models.vo.CouponVO;
import com.momsitter.models.vo.ExpireCouponVO;
import com.momsitter.models.vo.FirstPaymentEncourageVO;
import com.momsitter.models.vo.UserProductRemindVO;

public interface CronMapper {
    List<ExpireCouponVO> selectExpireCoupon();
    List<UserProductRemindVO> selectUserProductRemind();
    Integer insertNewCoupon(CouponVO coupon);
    List<FirstPaymentEncourageVO> selectFirstPaymentEncourageTarget();
}
