package com.momsitter.models.mapper;

import com.momsitter.models.vo.UserIdVO;
import com.momsitter.models.vo.UserScheduleVO;

import java.util.List;

public interface UserScheduleMapper {
    List<UserIdVO> selectUserIds();
    List<UserScheduleVO> selectUserScheduleByUserId(Integer userId);

}
