package com.momsitter.models.mapper;

import java.util.Map;

public interface UserWeekdayTimeIndexMapper {
    void deleteIndexByUserId(Integer userId);
    void insertIndex(Map<String, Object> param);
}
