package com.momsitter.models.mapper;

import com.momsitter.models.vo.MessageHistoryVO;

public interface MessageHistoryMapper {
    public boolean insertMessageHistory(MessageHistoryVO history);
}
