package com.momsitter.models.mapper;

import com.momsitter.models.vo.TimerTaskVO;
import com.momsitter.models.vo.TimerTaskWithTargetVO;

import java.util.List;

/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
public interface TimerTaskMapper {
    TimerTaskVO selectTimerTaskById(int timerTaskId);
    void updateTimerTaskStatusById(int timerTaskId, String status);
    TimerTaskVO insertTimerTask(Integer timerRuleId, String reservedDate);
    List<TimerTaskWithTargetVO> getPendingTimerTasks();
}
