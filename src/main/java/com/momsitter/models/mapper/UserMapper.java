package com.momsitter.models.mapper;

import com.momsitter.models.vo.*;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    UserRoleVO selectUserRole(Integer userId);
    UserVO selectUserByUserId(Integer userId);
    void updateParentProfileStatus(Map param);
    void updateSitterProfileStatus(Map param);

    List<RemindSitters> selectRemindSitter();

    ParentProfile selectParentProfileByUserId(Integer userId);
    SitterProfile selectSitterProfileByUserId(Integer userId);
    Integer selectUserWriteReviewCount(Integer userId);

    ReviewVO selectTargetReview(Map param);

    int findApplyIdManually(Map param);

}
