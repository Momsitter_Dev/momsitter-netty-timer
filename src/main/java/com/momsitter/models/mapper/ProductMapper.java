package com.momsitter.models.mapper;

import com.momsitter.models.vo.ProductVO;

import java.util.List;

public interface ProductMapper {
    List<ProductVO> getAllProduct();
}
