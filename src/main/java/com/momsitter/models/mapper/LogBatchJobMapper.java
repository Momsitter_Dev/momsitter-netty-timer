package com.momsitter.models.mapper;

import com.momsitter.models.vo.LogBatchJobVO;

public interface LogBatchJobMapper {
    void insertLogBatchJob(LogBatchJobVO log);
}
