package com.momsitter.models.mapper;

import com.momsitter.models.vo.TimerRuleVO;

import java.util.List;

/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
public interface TimerRuleMapper {
    public List<TimerRuleVO> getAll();
    public  TimerRuleVO selectTimerRuleById(Integer timerRuleId);
}
