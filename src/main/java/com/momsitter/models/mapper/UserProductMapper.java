package com.momsitter.models.mapper;

import com.momsitter.models.vo.UserProductVO;

import java.util.List;

public interface UserProductMapper {
    List<UserProductVO> selectNonExpiredUserProducts();
}
