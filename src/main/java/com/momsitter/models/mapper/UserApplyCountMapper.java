package com.momsitter.models.mapper;

public interface UserApplyCountMapper {
    void updateCountAsZero();
    void updateCountByUserId(Integer userId, Integer applyToSitterCount, Integer applyToParentCount);
}
