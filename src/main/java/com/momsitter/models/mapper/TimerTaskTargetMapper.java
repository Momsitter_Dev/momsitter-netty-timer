package com.momsitter.models.mapper;

import com.momsitter.models.vo.TimerTaskTargetVO;
import com.momsitter.models.vo.TimerTaskTargetUserVO;

import java.util.List;

/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
public interface TimerTaskTargetMapper {
    public List<TimerTaskTargetVO> selectTimerTaskTargetByTimerTaskId(Integer timerTaskId);
    public boolean insertTimerTaskTarget(Integer timerTaskId, Integer userId, String message, String buttonName, String buttonLink, Integer relatedUserId);
    public void insertTimerTaskTargets(Integer timerTaskId, List<Integer> userId);
    public List<TimerTaskTargetUserVO> selectTimerTaskTargetUserByTimerTaskId(Integer timerTaskId);
    public void updateTaskTargetResult(Integer taskTargetId, boolean result);
}
