package com.momsitter.models.vo;

public class FirstPaymentEncourageVO {
    private Integer couponId;
    private Integer userId;
    private String userName;
    private String userPhone;
    private String userStatus;
    private Integer productId;
    private String purchaseDate;
    private String createDate;
    private String updateDate;
    private Integer applyId;
    private String applyDate;
    private String targetDate1;
    private String targetDate2;

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getTargetDate1() {
        return targetDate1;
    }

    public void setTargetDate1(String targetDate1) {
        this.targetDate1 = targetDate1;
    }

    public String getTargetDate2() {
        return targetDate2;
    }

    public void setTargetDate2(String targetDate2) {
        this.targetDate2 = targetDate2;
    }
}
