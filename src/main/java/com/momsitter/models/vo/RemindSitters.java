package com.momsitter.models.vo;

public class RemindSitters {
    private Integer userId;
    private String userName;
    private String userKey;
    private String userEmail;
    private String userPhone;
    private Integer userTypeId;
    private String userStatus;
    private Integer profileId;
    private String profileStatus;
    private Integer applyCountId;
    private Integer applyToParentCount;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public Integer getApplyCountId() {
        return applyCountId;
    }

    public void setApplyCountId(Integer applyCountId) {
        this.applyCountId = applyCountId;
    }

    public Integer getApplyToParentCount() {
        return applyToParentCount;
    }

    public void setApplyToParentCount(Integer applyToParentCount) {
        this.applyToParentCount = applyToParentCount;
    }
}
