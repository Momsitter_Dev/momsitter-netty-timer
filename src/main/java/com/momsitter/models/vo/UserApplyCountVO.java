package com.momsitter.models.vo;

public class UserApplyCountVO {
    private Integer applyCountId;
    private Integer userId;
    private Integer applyToParentCount;
    private Integer applyToSitterCount;
    private String lastUpdated;

    public Integer getApplyCountId() {
        return applyCountId;
    }

    public void setApplyCountId(Integer applyCountId) {
        this.applyCountId = applyCountId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getApplyToParentCount() {
        return applyToParentCount;
    }

    public void setApplyToParentCount(Integer applyToParentCount) {
        this.applyToParentCount = applyToParentCount;
    }

    public Integer getApplyToSitterCount() {
        return applyToSitterCount;
    }

    public void setApplyToSitterCount(Integer applyToSitterCount) {
        this.applyToSitterCount = applyToSitterCount;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
