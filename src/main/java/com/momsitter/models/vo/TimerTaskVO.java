package com.momsitter.models.vo;

import java.util.Date;

/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
public class TimerTaskVO {
    private Integer timerTaskId;
    private Integer timerRuleId;
    private String regDate;
    private String reservedDate;
    private String excutedDate;
    private String status;

    public Integer getTimerTaskId() {
        return timerTaskId;
    }

    public void setTimerTaskId(Integer timerTaskId) {
        this.timerTaskId = timerTaskId;
    }

    public Integer getTimerRuleId() {
        return timerRuleId;
    }

    public void setTimerRuleId(Integer timerRuleId) {
        this.timerRuleId = timerRuleId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(String reservedDate) {
        this.reservedDate = reservedDate;
    }

    public String getExcutedDate() {
        return excutedDate;
    }

    public void setExcutedDate(String excutedDate) {
        this.excutedDate = excutedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
