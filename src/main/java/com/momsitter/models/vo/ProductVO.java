package com.momsitter.models.vo;

public class ProductVO {
    private Integer productId;
    private String productCode;
    private String productName;
    private String productDesc;
    private Integer productPrice;
    private String productType;
    private String productFor;
    private Integer productPeriod;
    private Integer productApplyToSitterCount;
    private Integer productApplyToParentCount;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public Integer getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductFor() {
        return productFor;
    }

    public void setProductFor(String productFor) {
        this.productFor = productFor;
    }

    public Integer getProductPeriod() {
        return productPeriod;
    }

    public void setProductPeriod(Integer productPeriod) {
        this.productPeriod = productPeriod;
    }

    public Integer getProductApplyToSitterCount() {
        return productApplyToSitterCount;
    }

    public void setProductApplyToSitterCount(Integer productApplyToSitterCount) {
        this.productApplyToSitterCount = productApplyToSitterCount;
    }

    public Integer getProductApplyToParentCount() {
        return productApplyToParentCount;
    }

    public void setProductApplyToParentCount(Integer productApplyToParentCount) {
        this.productApplyToParentCount = productApplyToParentCount;
    }
}
