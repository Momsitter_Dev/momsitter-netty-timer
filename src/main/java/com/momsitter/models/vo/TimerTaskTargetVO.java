package com.momsitter.models.vo;

/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
public class TimerTaskTargetVO {
    private Integer taskTargetId;
    private Integer timerTaskId;
    private Integer userId;
    private Integer relatedUserId;
    private String message;
    private String buttonName;
    private String buttonLink;
    private boolean result;

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonLink() {
        return buttonLink;
    }

    public void setButtonLink(String buttonLink) {
        this.buttonLink = buttonLink;
    }

    public Integer getRelatedUserId() {
        return relatedUserId;
    }

    public void setRelatedUserId(Integer relatedUserId) {
        this.relatedUserId = relatedUserId;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTaskTargetId() {
        return taskTargetId;
    }

    public void setTaskTargetId(Integer taskTargetId) {
        this.taskTargetId = taskTargetId;
    }

    public Integer getTimerTaskId() {
        return timerTaskId;
    }

    public void setTimerTaskId(Integer timerTaskId) {
        this.timerTaskId = timerTaskId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
