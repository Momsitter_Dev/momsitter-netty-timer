package com.momsitter.models.vo;

public class UserVO {
    private Integer userId;
    private String userKey;
    private String userSnsKey;
    private String userName;
    private String userPassword;
    private String userSignupDate;
    private String userBirthday;
    private Integer userAge;
    private String userConnectType;
    private String userPhone;
    private String userGender;
    private String userEmail;
    private String userDup;
    private boolean userNationality;
    private boolean userAssignmentTos;
    private boolean userAssignmentPp;
    private boolean userAssignmentMsg;
    private String userStatus;
    private boolean userSearchable;
    private String contactWay;
    private String contactWayDetail;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getUserSnsKey() {
        return userSnsKey;
    }

    public void setUserSnsKey(String userSnsKey) {
        this.userSnsKey = userSnsKey;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserSignupDate() {
        return userSignupDate;
    }

    public void setUserSignupDate(String userSignupDate) {
        this.userSignupDate = userSignupDate;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }

    public String getUserConnectType() {
        return userConnectType;
    }

    public void setUserConnectType(String userConnectType) {
        this.userConnectType = userConnectType;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDup() {
        return userDup;
    }

    public void setUserDup(String userDup) {
        this.userDup = userDup;
    }

    public boolean isUserNationality() {
        return userNationality;
    }

    public void setUserNationality(boolean userNationality) {
        this.userNationality = userNationality;
    }

    public boolean isUserAssignmentTos() {
        return userAssignmentTos;
    }

    public void setUserAssignmentTos(boolean userAssignmentTos) {
        this.userAssignmentTos = userAssignmentTos;
    }

    public boolean isUserAssignmentPp() {
        return userAssignmentPp;
    }

    public void setUserAssignmentPp(boolean userAssignmentPp) {
        this.userAssignmentPp = userAssignmentPp;
    }

    public boolean isUserAssignmentMsg() {
        return userAssignmentMsg;
    }

    public void setUserAssignmentMsg(boolean userAssignmentMsg) {
        this.userAssignmentMsg = userAssignmentMsg;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public boolean isUserSearchable() {
        return userSearchable;
    }

    public void setUserSearchable(boolean userSearchable) {
        this.userSearchable = userSearchable;
    }

    public String getContactWay() {
        return contactWay;
    }

    public void setContactWay(String contactWay) {
        this.contactWay = contactWay;
    }

    public String getContactWayDetail() {
        return contactWayDetail;
    }

    public void setContactWayDetail(String contactWayDetail) {
        this.contactWayDetail = contactWayDetail;
    }
}
