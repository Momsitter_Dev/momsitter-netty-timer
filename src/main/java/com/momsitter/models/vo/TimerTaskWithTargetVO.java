package com.momsitter.models.vo;

public class TimerTaskWithTargetVO {
    private Integer timerTaskId;
    private Integer timerRuleId;
    private String regDate;
    private String reservedDate;
    private String excutedDate;
    private String status;
    private Integer taskTargetId;
    private Integer userId;
    private Integer relatedUserId;
    private String message;
    private String buttonName;
    private String buttonLink;
    private boolean result;

    public Integer getTimerTaskId() {
        return timerTaskId;
    }

    public void setTimerTaskId(Integer timerTaskId) {
        this.timerTaskId = timerTaskId;
    }

    public Integer getTimerRuleId() {
        return timerRuleId;
    }

    public void setTimerRuleId(Integer timerRuleId) {
        this.timerRuleId = timerRuleId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getReservedDate() {
        return reservedDate;
    }

    public void setReservedDate(String reservedDate) {
        this.reservedDate = reservedDate;
    }

    public String getExcutedDate() {
        return excutedDate;
    }

    public void setExcutedDate(String excutedDate) {
        this.excutedDate = excutedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTaskTargetId() {
        return taskTargetId;
    }

    public void setTaskTargetId(Integer taskTargetId) {
        this.taskTargetId = taskTargetId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRelatedUserId() {
        return relatedUserId;
    }

    public void setRelatedUserId(Integer relatedUserId) {
        this.relatedUserId = relatedUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonLink() {
        return buttonLink;
    }

    public void setButtonLink(String buttonLink) {
        this.buttonLink = buttonLink;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
