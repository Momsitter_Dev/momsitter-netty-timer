package com.momsitter.models.vo;

public class TimerTaskTargetUserVO {
    Integer taskTargetId;
    Integer timerTaskId;
    String message;
    String buttonName;
    String buttonLink;
    String relatedUserId;
    boolean result;
    Integer userId;
    String userKey;
    String snsKey;
    String userName;
    String userPassword;
    String userSignUpDate;
    String userBirthday;
    Integer userAge;
    String userConnectType;
    String userPhone;
    String userGender;
    String userEmail;
    String userDup;
    String userNationality;
    String userAssignmentTos;
    String userAssignmentPp;
    String userAssignmentMsg;
    String userStatus;
    String userSearchable;
    String contactWay;
    String contactWayDetail;


    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonLink() {
        return buttonLink;
    }

    public void setButtonLink(String buttonLink) {
        this.buttonLink = buttonLink;
    }

    public String getRelatedUserId() {
        return relatedUserId;
    }

    public void setRelatedUserId(String relatedUserId) {
        this.relatedUserId = relatedUserId;
    }

    public Integer getTaskTargetId() {
        return taskTargetId;
    }

    public void setTaskTargetId(Integer taskTargetId) {
        this.taskTargetId = taskTargetId;
    }

    public Integer getTimerTaskId() {
        return timerTaskId;
    }

    public void setTimerTaskId(Integer timerTaskId) {
        this.timerTaskId = timerTaskId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getSnsKey() {
        return snsKey;
    }

    public void setSnsKey(String snsKey) {
        this.snsKey = snsKey;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserSignUpDate() {
        return userSignUpDate;
    }

    public void setUserSignUpDate(String userSignUpDate) {
        this.userSignUpDate = userSignUpDate;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }

    public String getUserConnectType() {
        return userConnectType;
    }

    public void setUserConnectType(String userConnectType) {
        this.userConnectType = userConnectType;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserDup() {
        return userDup;
    }

    public void setUserDup(String userDup) {
        this.userDup = userDup;
    }

    public String getUserNationality() {
        return userNationality;
    }

    public void setUserNationality(String userNationality) {
        this.userNationality = userNationality;
    }

    public String getUserAssignmentTos() {
        return userAssignmentTos;
    }

    public void setUserAssignmentTos(String userAssignmentTos) {
        this.userAssignmentTos = userAssignmentTos;
    }

    public String getUserAssignmentPp() {
        return userAssignmentPp;
    }

    public void setUserAssignmentPp(String userAssignmentPp) {
        this.userAssignmentPp = userAssignmentPp;
    }

    public String getUserAssignmentMsg() {
        return userAssignmentMsg;
    }

    public void setUserAssignmentMsg(String userAssignmentMsg) {
        this.userAssignmentMsg = userAssignmentMsg;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserSearchable() {
        return userSearchable;
    }

    public void setUserSearchable(String userSearchable) {
        this.userSearchable = userSearchable;
    }

    public String getContactWay() {
        return contactWay;
    }

    public void setContactWay(String contactWay) {
        this.contactWay = contactWay;
    }

    public String getContactWayDetail() {
        return contactWayDetail;
    }

    public void setContactWayDetail(String contactWayDetail) {
        this.contactWayDetail = contactWayDetail;
    }
}
