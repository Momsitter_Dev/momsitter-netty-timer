package com.momsitter.models.vo;

/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
public class TimerRuleVO {
    private Integer timerRuleId;
    private String timerRuleName;
    private String targetType;
    private String messageTemplateCode;
    private String messageType;

    public Integer getTimerRuleId() {
        return timerRuleId;
    }

    public void setTimerRuleId(Integer timerRuleId) {
        this.timerRuleId = timerRuleId;
    }

    public String getTimerRuleName() {
        return timerRuleName;
    }

    public void setTimerRuleName(String timerRuleName) {
        this.timerRuleName = timerRuleName;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getMessageTemplateCode() {
        return messageTemplateCode;
    }

    public void setMessageTemplateCode(String messageTemplateCode) {
        this.messageTemplateCode = messageTemplateCode;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}
