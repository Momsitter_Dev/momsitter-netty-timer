package com.momsitter.models.vo;

public class UserWeekdayTimeIndexVO {
    private Integer indexId;
    private Integer userId;
    private String startDate;
    private String endDate;
    private boolean mon;
    private boolean monMorning;
    private boolean moonNoon;
    private boolean moonNight;

    private boolean tue;
    private boolean tueMorning;
    private boolean tueNoon;
    private boolean tueNight;

    private boolean wed;
    private boolean wedMorning;
    private boolean wedNoon;
    private boolean wedNight;

    private boolean thu;
    private boolean thuMorning;
    private boolean thuNoon;
    private boolean thuNight;

    private boolean fri;
    private boolean friMorning;
    private boolean friNoon;
    private boolean friNight;

    private boolean sat;
    private boolean satMorning;
    private boolean satNoon;
    private boolean satNight;

    private boolean sun;
    private boolean sunMorning;
    private boolean sunNoon;
    private boolean sunNight;

    public Integer getIndexId() {
        return indexId;
    }

    public void setIndexId(Integer indexId) {
        this.indexId = indexId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isMon() {
        return mon;
    }

    public void setMon(boolean mon) {
        this.mon = mon;
    }

    public boolean isMonMorning() {
        return monMorning;
    }

    public void setMonMorning(boolean monMorning) {
        this.monMorning = monMorning;
    }

    public boolean isMoonNoon() {
        return moonNoon;
    }

    public void setMoonNoon(boolean moonNoon) {
        this.moonNoon = moonNoon;
    }

    public boolean isMoonNight() {
        return moonNight;
    }

    public void setMoonNight(boolean moonNight) {
        this.moonNight = moonNight;
    }

    public boolean isTue() {
        return tue;
    }

    public void setTue(boolean tue) {
        this.tue = tue;
    }

    public boolean isTueMorning() {
        return tueMorning;
    }

    public void setTueMorning(boolean tueMorning) {
        this.tueMorning = tueMorning;
    }

    public boolean isTueNoon() {
        return tueNoon;
    }

    public void setTueNoon(boolean tueNoon) {
        this.tueNoon = tueNoon;
    }

    public boolean isTueNight() {
        return tueNight;
    }

    public void setTueNight(boolean tueNight) {
        this.tueNight = tueNight;
    }

    public boolean isWed() {
        return wed;
    }

    public void setWed(boolean wed) {
        this.wed = wed;
    }

    public boolean isWedMorning() {
        return wedMorning;
    }

    public void setWedMorning(boolean wedMorning) {
        this.wedMorning = wedMorning;
    }

    public boolean isWedNoon() {
        return wedNoon;
    }

    public void setWedNoon(boolean wedNoon) {
        this.wedNoon = wedNoon;
    }

    public boolean isWedNight() {
        return wedNight;
    }

    public void setWedNight(boolean wedNight) {
        this.wedNight = wedNight;
    }

    public boolean isThu() {
        return thu;
    }

    public void setThu(boolean thu) {
        this.thu = thu;
    }

    public boolean isThuMorning() {
        return thuMorning;
    }

    public void setThuMorning(boolean thuMorning) {
        this.thuMorning = thuMorning;
    }

    public boolean isThuNoon() {
        return thuNoon;
    }

    public void setThuNoon(boolean thuNoon) {
        this.thuNoon = thuNoon;
    }

    public boolean isThuNight() {
        return thuNight;
    }

    public void setThuNight(boolean thuNight) {
        this.thuNight = thuNight;
    }

    public boolean isFri() {
        return fri;
    }

    public void setFri(boolean fri) {
        this.fri = fri;
    }

    public boolean isFriMorning() {
        return friMorning;
    }

    public void setFriMorning(boolean friMorning) {
        this.friMorning = friMorning;
    }

    public boolean isFriNoon() {
        return friNoon;
    }

    public void setFriNoon(boolean friNoon) {
        this.friNoon = friNoon;
    }

    public boolean isFriNight() {
        return friNight;
    }

    public void setFriNight(boolean friNight) {
        this.friNight = friNight;
    }

    public boolean isSat() {
        return sat;
    }

    public void setSat(boolean sat) {
        this.sat = sat;
    }

    public boolean isSatMorning() {
        return satMorning;
    }

    public void setSatMorning(boolean satMorning) {
        this.satMorning = satMorning;
    }

    public boolean isSatNoon() {
        return satNoon;
    }

    public void setSatNoon(boolean satNoon) {
        this.satNoon = satNoon;
    }

    public boolean isSatNight() {
        return satNight;
    }

    public void setSatNight(boolean satNight) {
        this.satNight = satNight;
    }

    public boolean isSun() {
        return sun;
    }

    public void setSun(boolean sun) {
        this.sun = sun;
    }

    public boolean isSunMorning() {
        return sunMorning;
    }

    public void setSunMorning(boolean sunMorning) {
        this.sunMorning = sunMorning;
    }

    public boolean isSunNoon() {
        return sunNoon;
    }

    public void setSunNoon(boolean sunNoon) {
        this.sunNoon = sunNoon;
    }

    public boolean isSunNight() {
        return sunNight;
    }

    public void setSunNight(boolean sunNight) {
        this.sunNight = sunNight;
    }
}
