package com.momsitter.models.vo;

public class UserProductRemindVO {
    private Integer userId;
    private String userPhone;
    private String userName;
    private String maxPurchaseDate;
    private String targetDate1;
    private String targetDate2;
    private String userStatus;

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMaxPurchaseDate() {
        return maxPurchaseDate;
    }

    public void setMaxPurchaseDate(String maxPurchaseDate) {
        this.maxPurchaseDate = maxPurchaseDate;
    }

    public String getTargetDate1() {
        return targetDate1;
    }

    public void setTargetDate1(String targetDate1) {
        this.targetDate1 = targetDate1;
    }

    public String getTargetDate2() {
        return targetDate2;
    }

    public void setTargetDate2(String targetDate2) {
        this.targetDate2 = targetDate2;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
}
