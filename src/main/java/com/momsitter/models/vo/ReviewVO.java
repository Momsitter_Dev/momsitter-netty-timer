package com.momsitter.models.vo;

public class ReviewVO {
    private Integer reviewId;
    private Integer applyId;
    private Integer senderId;
    private Integer targetId;
    private Integer reviewRate;
    private String reviewContent;
    private String reviewType;
    private String writeDate;
    private String interviewWay;
    private String status;

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public Integer getApplyId() {
        return applyId;
    }

    public void setApplyId(Integer applyId) {
        this.applyId = applyId;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public Integer getReviewRate() {
        return reviewRate;
    }

    public void setReviewRate(Integer reviewRate) {
        this.reviewRate = reviewRate;
    }

    public String getReviewContent() {
        return reviewContent;
    }

    public void setReviewContent(String reviewContent) {
        this.reviewContent = reviewContent;
    }

    public String getReviewType() {
        return reviewType;
    }

    public void setReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getWriteDate() {
        return writeDate;
    }

    public void setWriteDate(String writeDate) {
        this.writeDate = writeDate;
    }

    public String getInterviewWay() {
        return interviewWay;
    }

    public void setInterviewWay(String interviewWay) {
        this.interviewWay = interviewWay;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
