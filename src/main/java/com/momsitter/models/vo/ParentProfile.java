package com.momsitter.models.vo;

public class ParentProfile {
    private Integer profileId;
    private Integer userId;
    private Integer viewCount;
    private String createDate;
    private String updateDate;
    private String lastFinishedDate;
    private String profileStatus;
    private String profileTitle;
    private String profileDescription;
    private Integer wantedPayment;
    private String wantedInterviewWay;
    private String wantedSitterGender;
    private String wantedCareWay;
    private boolean longTerm;
    private boolean paymentNegotiable;
    private boolean durationNegotiable;
    private boolean weekdayNegotiable;
    private boolean timeNegotiable;
    private boolean moreThanThreeMonth;
    private Integer childCount;
    private Integer preferMinAge;
    private Integer preferMaxAge;

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getLastFinishedDate() {
        return lastFinishedDate;
    }

    public void setLastFinishedDate(String lastFinishedDate) {
        this.lastFinishedDate = lastFinishedDate;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getProfileTitle() {
        return profileTitle;
    }

    public void setProfileTitle(String profileTitle) {
        this.profileTitle = profileTitle;
    }

    public String getProfileDescription() {
        return profileDescription;
    }

    public void setProfileDescription(String profileDescription) {
        this.profileDescription = profileDescription;
    }

    public Integer getWantedPayment() {
        return wantedPayment;
    }

    public void setWantedPayment(Integer wantedPayment) {
        this.wantedPayment = wantedPayment;
    }

    public String getWantedInterviewWay() {
        return wantedInterviewWay;
    }

    public void setWantedInterviewWay(String wantedInterviewWay) {
        this.wantedInterviewWay = wantedInterviewWay;
    }

    public String getWantedSitterGender() {
        return wantedSitterGender;
    }

    public void setWantedSitterGender(String wantedSitterGender) {
        this.wantedSitterGender = wantedSitterGender;
    }

    public String getWantedCareWay() {
        return wantedCareWay;
    }

    public void setWantedCareWay(String wantedCareWay) {
        this.wantedCareWay = wantedCareWay;
    }

    public boolean isLongTerm() {
        return longTerm;
    }

    public void setLongTerm(boolean longTerm) {
        this.longTerm = longTerm;
    }

    public boolean isPaymentNegotiable() {
        return paymentNegotiable;
    }

    public void setPaymentNegotiable(boolean paymentNegotiable) {
        this.paymentNegotiable = paymentNegotiable;
    }

    public boolean isDurationNegotiable() {
        return durationNegotiable;
    }

    public void setDurationNegotiable(boolean durationNegotiable) {
        this.durationNegotiable = durationNegotiable;
    }

    public boolean isWeekdayNegotiable() {
        return weekdayNegotiable;
    }

    public void setWeekdayNegotiable(boolean weekdayNegotiable) {
        this.weekdayNegotiable = weekdayNegotiable;
    }

    public boolean isTimeNegotiable() {
        return timeNegotiable;
    }

    public void setTimeNegotiable(boolean timeNegotiable) {
        this.timeNegotiable = timeNegotiable;
    }

    public boolean isMoreThanThreeMonth() {
        return moreThanThreeMonth;
    }

    public void setMoreThanThreeMonth(boolean moreThanThreeMonth) {
        this.moreThanThreeMonth = moreThanThreeMonth;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public Integer getPreferMinAge() {
        return preferMinAge;
    }

    public void setPreferMinAge(Integer preferMinAge) {
        this.preferMinAge = preferMinAge;
    }

    public Integer getPreferMaxAge() {
        return preferMaxAge;
    }

    public void setPreferMaxAge(Integer preferMaxAge) {
        this.preferMaxAge = preferMaxAge;
    }
}
